package com.rep.vc.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class HttpClient {

    static private PoolingHttpClientConnectionManager conMan =
            new PoolingHttpClientConnectionManager();
    static private RequestConfig reqConfig = RequestConfig.custom()
            .setConnectTimeout(5000)
            .setConnectionRequestTimeout(5000)
            .setSocketTimeout(5000)
            .build();
    static CloseableHttpClient client = HttpClients.custom()
            .setDefaultRequestConfig(reqConfig)
            .setConnectionManager(conMan)
            .build();
    static {
        conMan.setMaxTotal(200);
        conMan.setDefaultMaxPerRoute(20);
    }

    public String get(String url) {
        MyValidations.isNotEmpty("url", url);
        try {
            HttpGet httpGet = new HttpGet(url);
            MyResponseHandler responseHandler = new MyResponseHandler();
            String response = client.execute(httpGet, responseHandler);
            return response;
        }  catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String post(String url, String bodyJsonStr) {
        MyValidations.isNotEmpty("url", url);
        MyValidations.isNotEmpty("bodyJsonStr", bodyJsonStr);

        try {
            HttpPost httpPost = new HttpPost(url);
            StringEntity entity = new StringEntity(bodyJsonStr, ContentType.APPLICATION_JSON);
            httpPost.setEntity(entity);
            MyResponseHandler responseHandler = new MyResponseHandler();
            return client.execute(httpPost, responseHandler);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String queryWorldstate(String networkId, String oid, String contractName, String stateKey) throws JsonProcessingException {
        MyValidations.isNotEmpty("networkId", networkId);
        MyValidations.isNotEmpty("contract", contractName);
        MyValidations.isNotEmpty("stateKey", stateKey);

        WorldstateQueryParam wqr =
                new WorldstateQueryParam(networkId, oid, contractName, stateKey);
        ObjectMapper om = new ObjectMapper();
        String wqrJsonStr = om.writeValueAsString(wqr);
        String url = Config.getInstance().getPeerEndpoint() + "/db/query";

        return post(url, wqrJsonStr);
    }

    public String queryDidPublicKey(String pubKeyId) throws JsonProcessingException, UnsupportedEncodingException {
        MyValidations.isNotEmpty("pubKeyId", pubKeyId);

        String url = Config.getInstance().getPeerEndpoint() + "/didPubKeys/" +
                URLEncoder.encode(pubKeyId, StandardCharsets.UTF_8.toString()
        );
        String res = get(url);
        JsonNode node = new ObjectMapper().readTree(res);
        String publicKeyPem = node.get("publicKeyPEM").asText();
        return publicKeyPem;
    }
}

class MyResponseHandler implements ResponseHandler<String> {
    @Override
    public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
        int status = response.getStatusLine().getStatusCode();
        if (status >= 200 && status < 300) {
            HttpEntity entity = response.getEntity();
            if (entity == null) return null;
            return EntityUtils.toString(entity);
        }
        return null;
    }
}

class WorldstateQueryParam {
    public WorldstateQueryParam(String networkId, String oid, String chaincodeName, String key) {
       this.netId = networkId;
       if (oid == null) {
           this.oid = "";
       } else {
           this.oid = oid;
       }
       this.chainCodeName = chaincodeName;
       this.key = key;
    }
    public String netId;
    public String oid;
    public String chainCodeName;
    public String key;
}

