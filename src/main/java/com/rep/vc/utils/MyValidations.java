package com.rep.vc.utils;

import java.security.InvalidParameterException;
import java.util.Map;
import java.util.regex.Pattern;

public class MyValidations {
    static public void isNotNull(String name, Object value) {
        if (value == null) {
            throw new InvalidParameterException("参数" + name + "不能为null");
        }
    }

    static public void isNotEmpty(String name, String value) {
        isNotNull(name, value);
        if (value.isEmpty()) {
            throw new InvalidParameterException("参数" + name + "不能为空字符串");
        }
        if (value.trim().isEmpty()) {
            throw new InvalidParameterException("参数" + name + "不能为空白字符串");
        }
    }

    static public <T> void isNotEmpty(String name, T[] value) {
        isNotNull(name, value);
        if (value.length == 0) {
            throw new InvalidParameterException("参数" + name + "不能为空数组");
        }
    }

    static public <K, V> void isNotEmpty(String name, Map<K, V> value) {
        isNotNull(name, value);
        if (value.size() == 0) {
            throw new InvalidParameterException("参数" + name + "不能为空Map");
        }
    }

    static public <T> void isGoodCollection(String name, T[] value) {
        isNotEmpty(name, value);
        for (T e : value) {
            if (e == null) {
                throw new InvalidParameterException("参数" + name + "不能含有null元素");
            }
        }
    }

    static public <K, V> void isGoodCollection(String name, Map<K, V> value) {
       isNotEmpty(name, value);
       for (Map.Entry<K, V> entry : value.entrySet()) {
           if( entry.getKey() == null || entry.getValue() == null) {
               throw new InvalidParameterException("参数" + name + "元素Key或Value不可为null");
           }
       }
    }

    static public void isValidDid(String did) {
        isNotEmpty("did", did);
        Boolean matched = Pattern.matches("^did:rep:.+", did);
        if (!matched) {
            throw new InvalidParameterException("参数不能是有效的did标识");
        }
    }

}
