package com.rep.vc.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * 全局可使用的变量
 * @author jayTsang
 */
public class Config {
    private Config() {
        if (instance == null) {
            instance = this;
        }
    }

    private static Config instance;

    public static Config getInstance() {
        if (instance == null) {
            return new Config();
        }
        return instance;
    }

    private String VCConstractName = "RVerifiableCredentialTPL";
    private Integer VCConstractVersion = 1;
    private String VCSignupCCSFunctionName = "signupCCS";
    private String VCUpdateCCSStatusFunctionName = "updateCCSStatus";
    private String VCSignupVCStatusFunctionName = "signupVCStatus";
    private String VCUpdateVCStatusFunctionName = "updateVCStatus";
    private String VCRevokeVCClaimsFunctionName = "revokeVCClaims";
    private String VCCCSWorldStatePrefix = "ccs-";
    private String VCVCStatusWorldStatePrefix = "vcs-";

    private String VCProtoVersion = "1.0";
    private String VCStatusProtoVersion = "1.0";

    private String TxSigAlg = "SHA256withECDSA";

    private String PeerEndpoint = "";

    private String NetworkId = "";

    private String OId = "";

    public String getVCConstractName() {
        return this.VCConstractName;
    }

    public void setVCConstractName(String VCConstractName) {
        this.VCConstractName = VCConstractName;
    }

    public Integer getVCConstractVersion() {
        return VCConstractVersion;
    }

    public void setVCConstractVersion(Integer VCConstractVersion) {
        this.VCConstractVersion = VCConstractVersion;
    }

    public String getVCSignupCCSFunctionName() {
        return VCSignupCCSFunctionName;
    }

    public void setVCSignupCCSFunctionName(String VCSignupCCSFunctionName) {
        this.VCSignupCCSFunctionName = VCSignupCCSFunctionName;
    }

    public String getVCUpdateCCSStatusFunctionName() {
        return VCUpdateCCSStatusFunctionName;
    }

    public void setVCUpdateCCSStatusFunctionName(String VCUpdateCCSStatusFunctionName) {
        this.VCUpdateCCSStatusFunctionName = VCUpdateCCSStatusFunctionName;
    }

    public String getVCSignupVCStatusFunctionName() {
        return VCSignupVCStatusFunctionName;
    }

    public void setVCSignupVCStatusFunctionName(String VCSignupVCStatusFunctionName) {
        this.VCSignupVCStatusFunctionName = VCSignupVCStatusFunctionName;
    }

    public String getVCUpdateVCStatusFunctionName() {
        return VCUpdateVCStatusFunctionName;
    }

    public void setVCUpdateVCStatusFunctionName(String VCUpdateVCStatusFunctionName) {
        this.VCUpdateVCStatusFunctionName = VCUpdateVCStatusFunctionName;
    }

    public String getVCRevokeVCClaimsFunctionName() {
        return VCRevokeVCClaimsFunctionName;
    }

    public void setVCRevokeVCClaimsFunctionName(String VCRevokeVCClaimsFunctionName) {
        this.VCRevokeVCClaimsFunctionName = VCRevokeVCClaimsFunctionName;
    }

    public String getVCCCSWorldStatePrefix() {
        return VCCCSWorldStatePrefix;
    }

    public void setVCCCSWorldStatePrefix(String VCCCSWorldStatePrefix) {
        this.VCCCSWorldStatePrefix = VCCCSWorldStatePrefix;
    }

    public String getVCVCStatusWorldStatePrefix() {
        return VCVCStatusWorldStatePrefix;
    }

    public void setVCVCStatusWorldStatePrefix(String VCVCStatusWorldStatePrefix) {
        this.VCVCStatusWorldStatePrefix = VCVCStatusWorldStatePrefix;
    }

    public String getTxSigAlg() {
        return TxSigAlg;
    }

    public void setTxSigAlg(String txSigAlg) {
        this.TxSigAlg = txSigAlg;
    }

    public String getPeerEndpoint() {
        return PeerEndpoint;
    }

    public String getPeerAddress() {
        Pattern pattern = Pattern.compile("^https?://(.*)$");
        Matcher matcher = pattern.matcher(PeerEndpoint);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        return null;
    }

    public void setPeerEndpoint(String peerEndpoint) {
        this.PeerEndpoint = peerEndpoint;
    }

    public String getVCProtoVersion() {
        return VCProtoVersion;
    }

    public String getVCStatusProtoVersion() {
        return VCStatusProtoVersion;
    }

    public void setVCStatusProtoVersion(String vcStatusProtoVersion) {
        this.VCStatusProtoVersion = vcStatusProtoVersion;
    }

    public void setVCProtoVersion(String vcProtoVersion) {
        this.VCProtoVersion = vcProtoVersion;
    }

    public String getNetworkId() {
        return NetworkId;
    }

    public void setNetworkId(String networkId) {
        this.NetworkId =  networkId;
    }

    public String getOId() {
        return OId;
    }

    public void setOId(String oid) {
        this.OId = oid;
    }

}
