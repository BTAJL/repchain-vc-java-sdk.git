package com.rep.vc.utils;

import com.rep.vc.models.proto.VCProto;
import com.rep.vc.models.vc.*;
import com.rep.vc.models.vp.VerifiablePresentation;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.StringReader;
import java.security.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 可验证凭据签名相关功能
 *
 * @author jayTsang
 */
public class SignatureTools {
    // 签名算法名称
    public static final String ECSecp256k1Sig = "EcdsaSecp256k1Signature";
    public static final String ECSecp256k1Sig2019 = "EcdsaSecp256k1Signature2019";
    public static final String ECPrime256v1Sig = "EcdsaPrime256v1Signature";
    public static final String RsaSig2018 = "RsaSignature2018";

    // 原子性签名，每个claim生成独立的签名
    public static final String ATOMIC_SIG_SCHEME = "automic_sig_scheme";

    // 为所有claims哈希值拼接后的哈希值签名
    public static final String ONE_HASH_SIG_SCHEME = "one_hash_sig_scheme";

    // 为所有claims哈希值构成的Merkle根签名
    public static final String MERKLE_SIG_SCHEME = "merkle_sig_scheme";



    public static String convertSigAlg(String sigAlg) {
        switch (sigAlg) {
            case ECSecp256k1Sig:
            case ECSecp256k1Sig2019:
            case ECPrime256v1Sig:
                return "SHA256withECDSA";
            case RsaSig2018:
                return "SHA256withRSA";
            default:
                throw new InvalidParameterException("不支持的签名算法: " + sigAlg);
        }
    }

    /**
     * 根据pem格式私钥获取私钥对象
     * @param prvKeyPem
     * @return 私钥对象
     * @throws IOException
     */
    public static PrivateKey getPrvKey(String prvKeyPem) throws IOException {
        StringReader sr = new StringReader(prvKeyPem);
        PEMParser p = new PEMParser(sr);
        Security.addProvider(new BouncyCastleProvider());
        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
        PrivateKey prvKey = converter.getPrivateKey(
                PrivateKeyInfo.getInstance(p.readObject())
        );
        return prvKey;
    }

    /**
     * 根据pem格式公钥获取公钥对象
     * @param pubKeyPem
     * @return 公钥对象
     * @throws IOException
     */
    public static PublicKey getPublicKey(String pubKeyPem) throws IOException {
        StringReader sr = new StringReader(pubKeyPem);
        PEMParser parser = new PEMParser(sr);
        Security.addProvider(new BouncyCastleProvider());
        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
        SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfo.getInstance(parser.readObject());
        PublicKey pubKey = converter.getPublicKey(publicKeyInfo);
        return pubKey;
    }

    public static byte[] sign2Bytes(String alg, PrivateKey prvKey, byte[] tbs) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature s = Signature.getInstance(alg);
        s.initSign(prvKey);
        s.update(tbs);
        byte[] res = s.sign();
        return res;
    }

    public static String sign2Base64(String alg, PrivateKey prvKey, byte[] tbs) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        byte[] resBytes = sign2Bytes(alg, prvKey, tbs);
        String res = Base64.encodeBase64String(resBytes);
        return res;
    }

    public static boolean verifySignature(String alg, PublicKey pubKey, byte[] tbs, byte[] signature) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature s = Signature.getInstance(alg);
        s.initVerify(pubKey);
        s.update(tbs);
        boolean res = s.verify(signature);
        return res;
    }

    public static boolean verifySignature(String alg, PublicKey publicKey, byte[] tbs, String signature) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        byte[] signatureBytes = Base64.decodeBase64(signature);
        return verifySignature(alg, publicKey, tbs, signatureBytes);
    }

//    public static String sign2Hex(String alg, PrivateKey prvKey, byte[] tbs) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
//        byte[] resBytes = sign2Bytes(alg, prvKey, tbs);
//        String res = Hex.encodeHexString(resBytes);
//        return res;
//    }

    public static void checkSigScheme(@Nonnull Credential credential, @Nonnull String sigScheme) {
        if ((credential instanceof CredentialAtomic && sigScheme != ATOMIC_SIG_SCHEME)
        || (!(credential instanceof CredentialAtomic) && sigScheme == ATOMIC_SIG_SCHEME)) {
            throw new IllegalArgumentException(String.format("签名模式:%s与credential类型不匹配", sigScheme));
        }
    }

    /**
     * 为每个claim产生独立的待签名信息tbs
     * @param vc
     * @return
     */
    public static Map<String, byte[]> generateAtomicVCTbs(@Nonnull VCProto.VerifiableCredential vc) {
        Map<String, byte[]> tbsMap = new HashMap<>();
        Map<String, VCProto.AtomicCredentialSubject.Claim> credentialSubject =
                vc.getAtomicCredentialSubject().getCredentialSubjectMap();
        for (Map.Entry<String, VCProto.AtomicCredentialSubject.Claim> entry
                : credentialSubject.entrySet()
        ) {
            VCProto.VerifiableCredential.Builder vcb =
                    VCProto.VerifiableCredential.newBuilder();
            vcb.mergeFrom(vc)
                    .clearAtomicCredentialSubject()
                    .getProofBuilder().clearAtomicSignature();
            VCProto.AtomicCredentialSubject.Builder acsb = VCProto.AtomicCredentialSubject.newBuilder();
            acsb.putCredentialSubject(entry.getKey(), entry.getValue());
            vcb.setAtomicCredentialSubject(acsb.build());
            tbsMap.put(entry.getKey(), vcb.build().toByteArray());
        }
        return tbsMap;
    }


    public static boolean verifyAtomicVerifiableCredential(@Nonnull VCProto.VerifiableCredential vc) throws IOException {
        if(!vc.hasProof() || !vc.getProof().hasAtomicSignature()) {
            return false;
        }

        // get pubKey from verificationMethod field of VCProof
        HttpClient client = new HttpClient();
        String pubKeyPem = client.queryDidPublicKey(vc.getProof().getVerificationMethod());
        PublicKey pubKey = getPublicKey(pubKeyPem);

        return verifyAtomicVerifiableCredential(vc, pubKey);
    }

    /**
     * 已经获得公钥的情况下，对可验证凭据的签名信息进行验证
     * @param vc
     * @param publicKey // 已获得的公钥
     * @return
     */
    public static boolean verifyAtomicVerifiableCredential(
            @Nonnull VCProto.VerifiableCredential vc,
            @Nonnull PublicKey publicKey
    ) {
        if(!vc.hasProof() || !vc.getProof().hasAtomicSignature()) {
            return false;
        }
        Map<String, byte[]> tbsMap = generateAtomicVCTbs(vc);
        try {
            String alg = convertSigAlg(vc.getProof().getType());
            Map<String, String> signatureMap = vc.getProof().getAtomicSignature().getSignatureMap();
            for (Map.Entry<String, String> signature : signatureMap.entrySet()) {
                if (!verifySignature(
                        alg,
                        publicKey,
                        tbsMap.get(signature.getKey()),
                        signature.getValue()
                )) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static byte[] generateVerifiablePresentationTbs(
            @Nonnull VCProto.VerifiablePresentation vp
    ) {
        VCProto.VerifiablePresentation.Builder vpb =
                VCProto.VerifiablePresentation.newBuilder();
        vpb.mergeFrom(vp)
                .getProofBuilder().clearSignature();
        byte[] tbs = vpb.build().toByteArray();
        return tbs;
    }

    public static boolean verifyVerifiablePresentation(@Nonnull VerifiablePresentation vp) throws IOException {
        if (vp.getProof() == null) {
            return false;
        }

        // get pubKey from verificationMethod field of VCProof
        HttpClient client = new HttpClient();
        String pubKeyPem = client.queryDidPublicKey(vp.getProof().getVerificationMethod());
        PublicKey pubKey = getPublicKey(pubKeyPem);

        return verifyVerifiablePresentation(vp, pubKey);
    }

    public static boolean verifyVerifiablePresentation(
            @Nonnull VerifiablePresentation vp,
            @Nonnull PublicKey pubKey
    ) {
        if (vp.getProof() == null) {
            return false;
        }
        String alg = convertSigAlg(vp.getProof().getType());
        byte[] tbs = generateVerifiablePresentationTbs(vp.getPvp());
        try {
            return verifySignature(alg, pubKey, tbs, vp.getProof().getSignature());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
