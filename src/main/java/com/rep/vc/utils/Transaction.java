package com.rep.vc.utils;

import com.rcjava.client.TranPostClient;
import com.rcjava.protos.Peer;
import com.rcjava.tran.impl.InvokeTran;
import com.rep.vc.roles.VCActivityRole;
import org.apache.commons.codec.binary.Hex;

/**
 * 提供RepChain交易相关功能的复用
 * @author jayTsang
 */
public class Transaction {
    static public class Contract {
        public Contract(
                String contractName, Integer contractVersion, String oid,
                String functionName, String argsJsonStr
        ) {
            this.contractName = contractName;
            this.contractVersion = contractVersion;
            this.oid= oid;
            this.functionName = functionName;
            this.argsJsonStr = argsJsonStr;
        }
        public String contractName;
        public Integer contractVersion;
        public String oid;
        public String functionName;
        public String argsJsonStr;
    }

    /**
     * 向RepChain网络提交交易
     * @param role
     * @param tpc
     * @param contract
     * @return 交易提交后RepChain节点返回的结果Json字符串
     * @throws Exception
     */
    static public String postTx(
            VCActivityRole role,
            TranPostClient tpc,
            Contract contract
    ) throws Exception {
        try {
            Peer.CertId certId = Peer.CertId.newBuilder()
                    .setCreditCode(role.getAccountId())
                    .setCertName(role.getCertName())
                    .build();
            Peer.ChaincodeId chaincodeId = Peer.ChaincodeId.newBuilder()
                    .setChaincodeName(contract.contractName)
                    .setVersion(contract.contractVersion)
                    .build();
            Peer.ChaincodeInput chaincodeInput = Peer.ChaincodeInput.newBuilder()
                    .setFunction(contract.functionName)
                    .addArgs(contract.argsJsonStr)
                    .build();
            InvokeTran it = InvokeTran.newBuilder()
                    .setCertId(certId)
                    .setChaincodeId(chaincodeId)
                    .setOid(contract.oid)
                    .setChaincodeInput(chaincodeInput)
                    .setPrivateKey(role.getPrvKey())
                    .setSignAlgorithm(Config.getInstance().getTxSigAlg())
                    .build();
            Peer.Transaction tx = it.getSignedTran();

            return tpc.postSignedTran(Hex.encodeHexString(tx.toByteArray()))
                    .toJSONString();
        } catch (Exception e) {
            throw new Exception(String.format("Got error when constructing and posting the RepChain transaction: %s", e.getMessage()));
        }
    }
}
