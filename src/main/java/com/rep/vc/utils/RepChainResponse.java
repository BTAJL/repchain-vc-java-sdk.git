package com.rep.vc.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RepChainResponse<T> {
    public T result;

    // For json deserialization
    public RepChainResponse() {}

    // 对于result值为可被解析为proto message的情形，获取相应json字符串
    public static String getProtoMsgJsonStr(String res) {
        Pattern pattern = Pattern.compile("^\\{[^\\{]*(\\{.*)\\}$");
        Matcher matcher = pattern.matcher(res);
        if (matcher.matches()) return matcher.group(1);
        return "";
    }
}
