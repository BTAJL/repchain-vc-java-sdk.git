package com.rep.vc.utils;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

public class Misc {
    public static String getCurrentISODateTime() {
        return ISODateTimeFormat.dateTime().withZoneUTC().print(DateTime.now());
    }
}
