package com.rep.vc.models.vc;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * 普通签名模式的凭据Credential，
 * 包含凭据元数据meta，以及描述对象信息credentialSubject，
 * 其credentialSubject是一个key为属性名称，value为属性值的Map对象
 *
 * @author jayTsang
 */
public class CredentialNormal extends Credential<Map<String, String>> {
    private Map<String, String> credentialSubject;

    public CredentialNormal() {}

    /**
     * CredentialNormal构造方法
     * @param meta 凭据元数据对象
     * @param credentialSubject 凭据描述对象信息
     */
    public CredentialNormal(
            @Nonnull VCMetadata meta,
            @Nonnull Map<String, String> credentialSubject
    ) {
       super(meta);
       this.credentialSubject = credentialSubject;
    }

    public Map<String, String> getCredentialSubject() {
        return credentialSubject;
    }

    public void setCredentialSubject(@Nonnull Map<String, String> credentialSubject) {
        this.credentialSubject = credentialSubject;
    }

    @Override
    public int getClaimCount() {
        return credentialSubject.size();
    }
}
