package com.rep.vc.models.vc;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class VerifiableCredentialDeserializer extends StdDeserializer<VerifiableCredential> {
    public VerifiableCredentialDeserializer() {
        this(null);
    }

    public VerifiableCredentialDeserializer(Class<?> t) {
        super(t);
    }
    @Override
    public VerifiableCredential deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = p.getCodec().readTree(p);

        VCMetadata metadata = new VCMetadata();
        JsonNode contextNode = node.get("@context");
        List<String> context = IntStream.range(0, contextNode.size()).boxed().map(
                i -> contextNode.get(i).asText()
        ).collect(Collectors.toList());
        metadata.setContext(context);
        metadata.setId(node.get("id").asText());
        JsonNode typeNode = node.get("type");
        List<String> type = IntStream.range(0, typeNode.size()).boxed().map(
                i -> typeNode.get(i).asText()
        ).collect(Collectors.toList());
        metadata.setType(type);
        metadata.setClaimScheme(node.get("claimScheme").asText());
        metadata.setIssuer(node.get("issuer").asText());
        metadata.setIssued(node.get("issued").asText());
        metadata.setValidFrom(node.get("validFrom").asText());
        metadata.setValidUntil(node.get("validUntil").asText());

        JsonNode credentialSubjectNode = node.get("credentialSubject");
        boolean isAtomic = credentialSubjectNode.fields().next().getValue().isObject();
        if (isAtomic) {
            Map<String, VCClaim> credentialSubject = new HashMap<>();
            Iterator<Map.Entry<String, JsonNode>> it = credentialSubjectNode.fields();
            while (it.hasNext()) {
                 Map.Entry<String, JsonNode> entry =
                         it.next();
                 Map.Entry<String, JsonNode> valueEntry =
                         entry.getValue().fields().next();
                 credentialSubject.put(
                         entry.getKey(),
                         new VCClaim(valueEntry.getKey(), valueEntry.getValue().asText())
                 );
            }
            Credential credential = new CredentialAtomic(metadata, credentialSubject);
            JsonNode proofNode = node.get("proof");
            String sigAlgType = proofNode.get("type").asText();
            String created = proofNode.get("created").asText();
            String verificationMethod = proofNode.get("verificationMethod").asText();
            Map<String, String> signature = new HashMap<>();
            it = proofNode.get("signature").fields();
            while (it.hasNext()) {
                Map.Entry<String, JsonNode> entry = it.next();
                signature.put(entry.getKey(), entry.getValue().asText());
            }
            VCProof proof = new VCProofAtomic(sigAlgType, created, verificationMethod, signature);
            VerifiableCredential vc = new VerifiableCredential(credential, proof);
            return vc;
        } else {
            // TODO: implementation
        }
        return null;
    }
}
