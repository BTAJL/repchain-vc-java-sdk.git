package com.rep.vc.models.vc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.protobuf.InvalidProtocolBufferException;
import com.rep.vc.models.proto.VCProto;
import com.rep.vc.models.vcstatus.VCStatus;
import com.rep.vc.utils.Config;
import com.rep.vc.utils.MyValidations;
import org.joda.time.DateTime;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static com.rep.vc.utils.Misc.getCurrentISODateTime;
import static com.rep.vc.utils.SignatureTools.*;

/**
 * 可验证凭据VerifiableCredential，主要包含凭据信息Credential和证明信息Proof，
 * 此外还含有使用Protobuf定义的可验证凭据pvc。
 * 属性pvc和本类实例对象互为镜像，
 * 其中属性pvc主要用于二进制格式可验证凭据的序列化和反序列化，
 * 以及签名验签过程中待签名数据TBS的生成；
 * 而本类实例对象主要用于json格式可验证凭据的序列化和反序列化
 *
 * @author jayTsang
 */
@JsonSerialize(using = VerifiableCredentialSerializer.class)
@JsonDeserialize(using = VerifiableCredentialDeserializer.class)
public class VerifiableCredential {
    private Credential<?> credential;
    private VCProof<?> proof;
    private VCProto.VerifiableCredential pvc;

    public VerifiableCredential() {}

    /**
     * VerifiableCredential构造方法，用于从json字符串反序列化，
     * 使用者应使用其他构造方法
     * @param credential 凭据对象
     * @param proof 凭据证明信息对象
     */
    @SuppressWarnings("unchecked")
    public VerifiableCredential(
            @Nonnull Credential<?> credential,
            @Nonnull VCProof<?> proof
    ) {
        this.credential = credential;
        this.proof = proof;
        VCProto.VerifiableCredential.Builder vcb =
                this.generateVCProtoBuilder(credential);
        VCProto.VCProof.Builder vcpb = VCProto.VCProof.newBuilder();
        vcpb.setType(proof.getType())
                .setCreated(proof.getCreated())
                .setVerificationMethod(proof.getVerificationMethod());
        if (vcb.hasAtomicCredentialSubject()) {
            VCProto.AtomicSignature.Builder asb = VCProto.AtomicSignature.newBuilder();
            asb.putAllSignature((Map<String, String>) proof.getSignature());
            vcpb.setAtomicSignature(asb.build());

        } else {
            VCProto.NormalSignature.Builder nsb = VCProto.NormalSignature.newBuilder();
            nsb.setSignature((String) proof.getSignature());
            vcpb.setNormalSignature(nsb.build());
        }
        vcb.setProof(vcpb.build());
        this.pvc = vcb.build();
        // TODO: verifySignature
    }

    /**
     * VerifiableCredential构造方法，
     * 可验证凭据初始状态没有被签名，在签名时将自动生成proof属性
     * @param credential 凭据对象
     */
    @SuppressWarnings("unchecked")
    public VerifiableCredential(@Nonnull Credential<?> credential) {
        this.credential = credential;

        // create VerifiableCredential proto message without proof
        this.pvc = this.generateVCProtoBuilder(credential).build();
    }

    /**
     * 生成可验证凭据Protobuf Builder
     * @param credential 凭据对象
     * @return
     */
    private VCProto.VerifiableCredential.Builder generateVCProtoBuilder(Credential<?> credential) {
        VCProto.VerifiableCredential.Builder vcb =
                VCProto.VerifiableCredential.newBuilder();
        vcb.setVersion(Config.getInstance().getVCProtoVersion())
                .addAllContext(credential.getMeta().getContext())
                .setId(credential.getMeta().getId())
                .addAllType(credential.getMeta().getType())
                .setClaimScheme(credential.getMeta().getClaimScheme())
                .setIssuer(credential.getMeta().getIssuer())
                .setIssued(credential.getMeta().getIssued())
                .setValidFrom(credential.getMeta().getValidFrom())
                .setValidUntil(credential.getMeta().getValidUntil());
        if (credential instanceof CredentialAtomic) {
            VCProto.AtomicCredentialSubject.Builder acsb =
                    VCProto.AtomicCredentialSubject.newBuilder();
            for (Map.Entry<String, VCClaim> claim :
                    ((Map<String, VCClaim>) credential.getCredentialSubject()).entrySet()
            ) {
                VCProto.AtomicCredentialSubject.Claim.Builder cb =
                        VCProto.AtomicCredentialSubject.Claim.newBuilder();
                cb.setName(claim.getValue().getName())
                        .setValue(claim.getValue().getValue());
                acsb.putCredentialSubject(claim.getKey(), cb.build());
            }
            vcb.setAtomicCredentialSubject(acsb.build());
        } else {
            VCProto.NormalCredentialSubject.Builder nsb =
                    VCProto.NormalCredentialSubject.newBuilder();
            nsb.putAllCredentialSubject((Map<String, String>) credential.getCredentialSubject());
            vcb.setNormalCredentialSubject(nsb.build());
        }
        return vcb;
    }

    /**
     * 使用原子签名模式对凭据进行签名
     * @param prvKey 签名所用私钥对象
     * @param pubKeyId 验证签名时应使用的公钥的标识
     * @param sigAlg 签名算法，目前支持EcdsaSecp256k1Signature/EcdsaSecp256k1Signature2019/EcdsaPrime256v1Signature/RsaSignature2018
     * @throws Exception
     */
    public void sign(
            @Nonnull PrivateKey prvKey,
            @Nonnull String pubKeyId,
            @Nonnull String sigAlg
    ) throws Exception {
        this.sign(prvKey, pubKeyId, sigAlg, ATOMIC_SIG_SCHEME);
    }

    /**
     * 对凭据进行签名，生成可验证凭据
     * @param prvKey 签名所用私钥对象
     * @param pubKeyId 验证签名时应使用的公钥的标识
     * @param sigAlg 签名算法，目前支持EcdsaSecp256k1Signature/EcdsaSecp256k1Signature2019/EcdsaPrime256v1Signature/RsaSignature2018
     * @param sigScheme 签名模式，目前支持automic_sig_scheme(原子模式)/merkle_sig_scheme(merkle模式)
     * @throws Exception
     */
    public void sign(
            @Nonnull PrivateKey prvKey,
            @Nonnull String pubKeyId,
            @Nonnull String sigAlg,
            @Nonnull String sigScheme
    ) throws Exception {
        if (this.proof != null) {
            throw new Exception("不能对已被签名的可验证凭据签名");
        }
        MyValidations.isNotEmpty("pubKeyId", pubKeyId);
        MyValidations.isNotEmpty("sigAlg", sigAlg);
        MyValidations.isNotEmpty("sigScheme", sigScheme);
        String alg = convertSigAlg(sigAlg);
        checkSigScheme(this.credential, sigScheme);
        switch (sigScheme) {
            case ATOMIC_SIG_SCHEME: {
                VCProto.VCProof.Builder pb = VCProto.VCProof.newBuilder();
                String created = getCurrentISODateTime();
                pb.setType(sigAlg)
                        .setCreated(created)
                        .setVerificationMethod(pubKeyId);
                VCProto.VerifiableCredential.Builder vcb =
                        VCProto.VerifiableCredential.newBuilder();
                vcb.mergeFrom(this.pvc).setProof(pb.build());
                // The signature includes the fields of the proof except the signature field
                Map<String, byte[]> tbsMap = generateAtomicVCTbs(vcb.build());
                Map<String, String> signature = new HashMap<>();
                for (Map.Entry<String, byte[]> tbs : tbsMap.entrySet()) {
                    signature.put(
                            tbs.getKey(),
                            sign2Base64(alg, prvKey, tbs.getValue())
                    );
                }
                VCProofAtomic proof = new VCProofAtomic(sigAlg, created, pubKeyId, signature);
                this.proof = proof;

                VCProto.AtomicSignature.Builder asb = VCProto.AtomicSignature.newBuilder();
                asb.putAllSignature(signature);
                vcb.getProofBuilder().setAtomicSignature(asb.build());

                this.pvc = vcb.build();
                break;
            }
            case MERKLE_SIG_SCHEME:
            case ONE_HASH_SIG_SCHEME:
                // TODO: implementation
                break;
            default :
                throw new InvalidParameterException("无效的签名模式: " + sigScheme);
        }
    }

    /**
     * 验证可验证凭据中的签名信息，
     * 根据属性proof中的验证公钥Id自动向RepChain获取公钥信息
     * @return 返回验签是否成功，true表示成功，false表示失败
     */
    public boolean verifySignature() throws IOException {
        if (this.pvc.hasAtomicCredentialSubject()) {
            return verifyAtomicVerifiableCredential(this.pvc);
        }
        // TODO: implementation
        return false;
    }

    /**
     * 验证可验证凭据中的签名信息
     * 根据传入的公钥信息进行验签
     * @param publicKey 验签公钥对象
     * @return 返回验签是否成功，true表示成功，false表示失败
     */
    public boolean verifySignature(@Nonnull PublicKey publicKey) {
        if (this.pvc.hasAtomicCredentialSubject()) {
            return verifyAtomicVerifiableCredential(this.pvc, publicKey);
        }
        // TODO: implementation
        return false;
    }

    /**
     * 验证可验证凭据是否处于有效期限内
     * @return 返回验证是否成功，true表示成功，false表示失败
     */
    public boolean verifyInValidPeriod() {
        if (DateTime.parse(this.credential.getMeta().getValidFrom()).isAfterNow()) {
            return false;
        }
        if (DateTime.parse(this.credential.getMeta().getValidUntil()).isBeforeNow()) {
            return false;
        }
        return true;
    }

    /**
     * 验证可验证凭据的状态以及属性的有效性，将从RepChain中获取相应信息
     * @param targetStatus 期望可验证凭据处于的目标状态
     * @return 返回验证是否成功，true表示成功，false表示失败
     */
    public boolean verifyStatusAndClaims(@Nonnull String targetStatus) {
        VCStatus status = VCStatus.getVCStatusFromRepChain(this.getCredential().getMeta().getId());
        if (status == null) {
            return false;
        }
        return targetStatus.equals(status.getStatus())
                && status.getCreator().equals(this.getCredential().getMeta().getIssuer())
                && verifyNoRevokedClaim(status);
    }

    /**
     * 验证可验证凭据中的属性是否均有效，即未被撤销
     * @param status 可验证凭据状态对象
     * @return 返回验证是否成功，true表示成功，false表示失败
     */
    private boolean verifyNoRevokedClaim(VCStatus status) {
        if (this.getCredential() instanceof CredentialAtomic) {
            HashSet<String> revokedClaimIndex =
                    new HashSet<>(Arrays.asList(status.getRevokedClaimIndex()));
            revokedClaimIndex.retainAll(
                    ((Map<String, VCClaim>) this.getCredential().getCredentialSubject()).keySet()
            );
            if(revokedClaimIndex.size() > 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * 将可验证凭据导出为二进制格式
     * @return 二进制格式的可验证凭据
     */
    public byte[] exportAsBytes() {
        return this.pvc.toByteArray();
    }

    /**
     * 将可验证凭据导出为Json字符串
     * @return Json字符串格式的可验证凭据
     * @throws JsonProcessingException
     */
    public String exportAsJsonStr() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    /**
     * 将Protobuf对象解析为可验证凭据对象
     * @param message 可验证凭据的Protobuf对象
     * @return 可验证凭据对象
     */
    public static VerifiableCredential parseFrom(
            @Nonnull VCProto.VerifiableCredential message
    ) {
        Credential credential;
        VCMetadata metadata = new VCMetadata();
        metadata.setContext(message.getContextList());
        metadata.setType(message.getTypeList());
        metadata.setId(message.getId());
        metadata.setClaimScheme(message.getClaimScheme());
        metadata.setIssuer(message.getIssuer());
        metadata.setIssued(message.getIssued());
        metadata.setValidUntil(message.getValidUntil());
        metadata.setValidFrom(message.getValidFrom());
        if (message.hasAtomicCredentialSubject()) {
            if (!message.getProof().hasAtomicSignature()) {
                throw new InvalidParameterException(
                        "无效参数：message，其属性credentialSubject类型与签名模式不匹配"
                );
            }

            Map<String, VCClaim> credentialSubject = new HashMap<>();
            for (Map.Entry<String, VCProto.AtomicCredentialSubject.Claim> claim :
                    message.getAtomicCredentialSubject().getCredentialSubjectMap().entrySet()
            ) {
                String number = claim.getKey();
                String name = claim.getValue().getName();
                String value = claim.getValue().getValue();
                credentialSubject.put(number, new VCClaim(name, value));
            }
            credential = new CredentialAtomic(metadata, credentialSubject);

            VerifiableCredential vc = new VerifiableCredential();
            vc.setPvc(message);
            vc.setCredential(credential);

            if (!message.hasProof()) {
               vc.setCredential(credential);
               return vc;
            }

            VCProofAtomic proof = new VCProofAtomic();
            proof.setType(message.getProof().getType());
            proof.setCreated(message.getProof().getCreated());
            proof.setVerificationMethod(message.getProof().getVerificationMethod());
            proof.setSignature(message.getProof().getAtomicSignature().getSignatureMap());
            vc.setProof(proof);
            // TODO: log
            return vc;
        } else if (message.hasNormalCredentialSubject()) {
            Map<String, String> credentialSubject = new HashMap<>();
            for (Map.Entry<String, String> claim :
                    message.getNormalCredentialSubject().getCredentialSubjectMap().entrySet()
            ) {
                String name = claim.getKey();
                String value = claim.getValue();
                credentialSubject.put(name, value);
            }
            credential = new CredentialNormal(metadata, credentialSubject);

            VerifiableCredential vc = new VerifiableCredential();
            vc.setPvc(message);
            vc.setCredential(credential);

            if (!message.hasProof()) {
                vc.setCredential(credential);
                return vc;
            }
            if (!message.getProof().hasNormalSignature()) {
                throw new InvalidParameterException("无效参数：message");
            }
            VCProofNormal proof = new VCProofNormal();
            proof.setType(message.getProof().getType());
            proof.setCreated(message.getProof().getCreated());
            proof.setVerificationMethod(message.getProof().getVerificationMethod());
            proof.setSignature(message.getProof().getNormalSignature().getSignature());
            vc.setProof(proof);
            // TODO: log
            return vc;
        } else {
            throw new InvalidParameterException("无效的参数: message");
        }
    }

    /**
     * 将Json字符串解析为可验证凭据对象
     * @param vcJsonStr Json字符串格式的可验证凭据
     * @return 可验证凭据对象
     * @throws JsonProcessingException
     */
    public static VerifiableCredential parseFrom(@Nonnull String vcJsonStr) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(vcJsonStr, VerifiableCredential.class);
    }

    /**
     * 将二进制数据解析为可验证凭据对象
     * @param vcProtoBytes 二进制格式的可验证凭据
     * @return
     */
    public static VerifiableCredential parseFrom(byte[] vcProtoBytes) {
        try {
            VCProto.VerifiableCredential message =
                    VCProto.VerifiableCredential.parseFrom(vcProtoBytes);
            return parseFrom(message);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            // TODO: log
        }
        return null;
    }

    public Credential<?> getCredential() {
        return credential;
    }

    private void setCredential(@Nonnull Credential<?> credential) {
        this.credential = credential;
    }

    public VCProof<?> getProof() {
        return proof;
    }

    private void setProof(VCProof<?> proof) {
        this.proof = proof;
    }

    public VCProto.VerifiableCredential getPvc() {
        return pvc;
    }

    private void setPvc(VCProto.VerifiableCredential pvc) {
        this.pvc = pvc;
    }
}
