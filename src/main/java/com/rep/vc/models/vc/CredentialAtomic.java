package com.rep.vc.models.vc;

import javax.annotation.Nonnull;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

/**
 * 原子性签名模式的凭据Credential，
 * 包含凭据元数据meta，以及描述对象信息credentialSubject，
 * 其credentialSubject是一个key为属性编号，value为属性VCClaim对象的Map对象
 *
 * @author jayTsang
 */
public class CredentialAtomic extends Credential<Map<String, VCClaim>> {
    private Map<String, VCClaim> credentialSubject;
    private Map<String, String> nameMap2Number;

    public CredentialAtomic() {}

    /**
     * CredentialAtomic构造方法
     * @param meta 凭据元数据对象
     * @param credentialSubject 凭据描述对象信息
     */
    public CredentialAtomic(
            @Nonnull VCMetadata meta,
            @Nonnull Map<String, VCClaim> credentialSubject
    ) {
        super(meta);
        this.credentialSubject = credentialSubject;
        nameMap2Number = new HashMap<>();
        for (Map.Entry<String, VCClaim> entry : credentialSubject.entrySet()) {
            // assert that the name of a claim is unique
            if (this.nameMap2Number.containsKey(entry.getValue().getName()))  {
                throw new InvalidParameterException("参数credentialSubject中不能含有相同name值的claim");
            }
            // construct a map of claim name to its number
            this.nameMap2Number.put(entry.getValue().getName(), entry.getKey());
        }
    }

    public Map<String, VCClaim> getCredentialSubject() {
        return credentialSubject;
    }

    public void setCredentialSubject(@Nonnull Map<String, VCClaim> credentialSubject) {
        this.credentialSubject = credentialSubject;
    }

    public Map<String, String> getNameMap2Number() {
        return nameMap2Number;
    }

    @Override
    public int getClaimCount() {
        return credentialSubject.size();
    }
}
