package com.rep.vc.models.vc;

import com.rep.vc.utils.MyValidations;

/**
 * 可验证凭据proof部分虚基类
 * @param <T>
 *
 * @author jayTsang
 */
abstract public class VCProof<T> {
    private String type;
    private String created;
    private String verificationMethod;

    public VCProof() {}

    public VCProof(
            String type,
            String created,
            String verificationMethod
    ) {
        MyValidations.isNotEmpty("type", type);
        MyValidations.isNotEmpty("created", created);
        MyValidations.isNotEmpty("verificationMethod", verificationMethod);
        this.type = type;
        this.created = created;
        this.verificationMethod = verificationMethod;
    }

    abstract public T getSignature();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getVerificationMethod() {
        return verificationMethod;
    }

    public void setVerificationMethod(String verificationMethod) {
        this.verificationMethod = verificationMethod;
    }
}
