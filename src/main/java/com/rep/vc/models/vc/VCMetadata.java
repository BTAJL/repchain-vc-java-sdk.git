package com.rep.vc.models.vc;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 可验证凭据元数据VCMetadata
 *
 * @author jayTsang
 */
public class VCMetadata {
    /** 对应ｗ3c vc-data-model标准中的＠context */
    private List<String> context =
            new ArrayList<>(Arrays.asList(new String[]{"https://www.w3.org/2018/credentials/v1"}));
    /** 可验证凭据唯一标识 */
    private String id;
    /** 可验证凭据类型 */
    private List<String> type =
            new ArrayList<>(Arrays.asList(new String[]{"VerifiableCredential"}));
    /** 可验证凭据属性结构唯一标识 */
    private String claimScheme;
    /** 可验证凭据签发者 */
    private String issuer;
    /** 可验证凭据签发时间 */
    private String issued;
    /** 可验证凭据有效起始时间 */
    private String validFrom;
    /** 可验证凭据有效终止时间 */
    private String validUntil;

    public VCMetadata() {}

    public List<String> getContext() {
        return context;
    }

    /**
     * 设置context的值，根据ｗ3c vc-data-model标准，
     * 其第一个元素应固定为: "https://www.w3.org/2018/credentials/v1"
     * @param context
     */
    public void setContext(@Nonnull List<String> context) {
        this.context = context;
    }

    public void addContext(@Nonnull String context) {
        this.context.add(context);
    }

    public void addContext(@Nonnull int index, @Nonnull String context) {
        this.context.add(index, context);
    }

    public String getId() {
        return id;
    }

    public void setId(@Nonnull String id) {
        this.id = id;
    }

    public List<String> getType() {
        return type;
    }

    /**
     * 设置type的值，根据ｗ3c vc-data-model标准，
     * 其第一个元素应固定为: "VerifiableCredential"
     * @param type
     */
    public void setType(@Nonnull List<String> type) {
        this.type = type;
    }

    public void addType(@Nonnull String type) {
        this.type.add(type);
    }

    public void addType(@Nonnull int index, @Nonnull String type) {
        this.type.add(index, type);
    }

    public String getClaimScheme() {
        return claimScheme;
    }

    public void setClaimScheme(@Nonnull String claimScheme) {
        this.claimScheme = claimScheme;
    }

    public String getIssuer() {
        return issuer;
    }

    // TODO: 不应被外部调用
    public void setIssuer(@Nonnull String issuer) {
        this.issuer = issuer;
    }

    public String getIssued() {
        return issued;
    }

    // TODO: 不应被外部调用
    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(@Nonnull String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(@Nonnull String validUntil) {
        this.validUntil = validUntil;
    }
}
