package com.rep.vc.models.vc;

import com.rep.vc.utils.MyValidations;

/**
 * 原子签名模式下的凭据描述对象的属性结构
 *
 * @author jayTsang
 */
public class VCClaim {
    // Claim属性名
    String name;
    // Claim属性值
    String value;

    public VCClaim() {}

    /**
     * VCClaim构造方法
     * @param name 属性名称
     * @param value 属性值
     */
    public VCClaim(String name, String value) {
        MyValidations.isNotEmpty("name", name);
        MyValidations.isNotEmpty("value", value);
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
