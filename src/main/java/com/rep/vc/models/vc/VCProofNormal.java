package com.rep.vc.models.vc;

import com.rep.vc.utils.MyValidations;

/**
 * 普通签名模式的可验证凭据proof
 * 其签名数据signature是一个Base64编码的字符串
 *
 * @author jayTsang
 */
public class VCProofNormal extends VCProof<String> {
    private String signature;

    public VCProofNormal() {}

    /**
     * VCProofNormal构造方法
     * @param type 签名算法，目前支持EcdsaSecp256k1Signature/EcdsaSecp256k1Signature2019/EcdsaPrime256v1Signature/RsaSignature2018
     * @param created 签名时间
     * @param verificationMathod 验签公钥标识，通过did uri表示的公钥标识
     * @param signature 签名数据，Base64编码的字符串
     */
    public VCProofNormal(
            String type,
            String created,
            String verificationMathod,
            String signature
    ) {
        super(type, created, verificationMathod);
        MyValidations.isNotEmpty(signature, signature);
        this.signature = signature;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
