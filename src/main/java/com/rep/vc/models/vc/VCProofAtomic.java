package com.rep.vc.models.vc;

import com.rep.vc.utils.MyValidations;

import java.util.Map;

/**
 * 基于原子性签名模式的可验证凭据proof
 * 其签名数据signature是一个key为属性编号，value为相应原子性签名的Map对象
 *
 * @author jayTsang
 */
public class VCProofAtomic extends VCProof<Map<String, String>> {
    private Map<String, String> signature;

    public VCProofAtomic() {}

    /**
     * VCProofAtomic构造方法
     * @param type 签名算法，目前支持EcdsaSecp256k1Signature/EcdsaSecp256k1Signature2019/EcdsaPrime256v1Signature/RsaSignature2018
     * @param created 初始签名时间
     * @param verificationMethod 验签公钥标识，通过did uri表示的公钥标识
     * @param signature 签名数据，每个属性编号对应一个原子签名数据，每个签名数据用Base64编码字符串表示
     */
    public VCProofAtomic(
            String type,
            String created,
            String verificationMethod,
            Map<String, String> signature
    ) {
        super(type, created, verificationMethod);
        MyValidations.isGoodCollection("signature", signature);
        this.signature = signature;
    }

    public Map<String, String> getSignature() {
        return this.signature;
    }

    public void setSignature(Map<String, String> signature) {
        MyValidations.isGoodCollection("signature", signature);
        this.signature = signature;
    }
}
