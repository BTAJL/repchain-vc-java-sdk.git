package com.rep.vc.models.vc;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Map;

public class VerifiableCredentialSerializer extends StdSerializer<VerifiableCredential> {

    public VerifiableCredentialSerializer() {
        this(null);
    }

    public VerifiableCredentialSerializer(Class<VerifiableCredential> t) {
        super(t);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void serialize(VerifiableCredential value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        Credential credential = value.getCredential();
        VCMetadata metadata = credential.getMeta();
        VCProof proof = value.getProof();
        String[] context = new String[metadata.getContext().size()];
        metadata.getContext().toArray(context);
        String[] type = new String[metadata.getType().size()];
        metadata.getType().toArray(type);
        gen.writeStartObject();
        gen.writeFieldName("@context");
        gen.writeArray(context, 0, context.length);
        gen.writeStringField("id", metadata.getId());
        gen.writeFieldName("type");
        gen.writeArray(type, 0, type.length);
        gen.writeStringField("claimScheme", metadata.getClaimScheme());
        gen.writeStringField("issuer", metadata.getIssuer());
        gen.writeStringField("issued", metadata.getIssued());
        gen.writeStringField("validFrom", metadata.getValidFrom());
        gen.writeStringField("validUntil", metadata.getValidUntil());
        if (credential instanceof CredentialAtomic) {
            gen.writeFieldName("credentialSubject");
            gen.writeStartObject();
            for (Map.Entry<String, VCClaim> claim :
                    ((Map<String, VCClaim>) credential.getCredentialSubject()).entrySet()) {
                gen.writeFieldName(claim.getKey());
                gen.writeStartObject();
                gen.writeStringField(claim.getValue().getName(), claim.getValue().getValue());
                gen.writeEndObject();
            }
            gen.writeEndObject();

            gen.writeFieldName("proof");
            gen.writeStartObject();
            gen.writeStringField("type", proof.getType());
            gen.writeStringField("created", proof.getCreated());
            gen.writeStringField("verificationMethod", proof.getVerificationMethod());
            gen.writeFieldName("signature");
            gen.writeStartObject();
            for (Map.Entry<String, String> signature :
                    ((Map<String, String>) proof.getSignature()).entrySet()) {
                gen.writeStringField(signature.getKey(), signature.getValue());
            }
            gen.writeEndObject();
            gen.writeEndObject();
        } else if (credential instanceof CredentialNormal) {
            gen.writeFieldName("credentialSubject");
            gen.writeStartObject();
            for (Map.Entry<String, String> claim :
                    ((Map<String, String>) credential.getCredentialSubject()).entrySet()) {
                gen.writeStringField(claim.getKey(), claim.getValue());
            }
            gen.writeEndObject();

            gen.writeFieldName("proof");
            gen.writeStartObject();
            gen.writeStringField("type", proof.getType());
            gen.writeStringField("created", proof.getCreated());
            gen.writeStringField("verificationMethod", proof.getVerificationMethod());
            gen.writeStringField("signature", (String)proof.getSignature());
            gen.writeEndObject();
        } else {
            throw new IOException("无法Json序列化该VerifiableCredential对象");
        }
        gen.writeEndObject();
    }
}
