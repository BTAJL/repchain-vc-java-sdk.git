package com.rep.vc.models.vc;

import javax.annotation.Nonnull;

/**
 * 凭据Credential虚基类
 * @param <T>
 *
 * @author jayTsang
 */
abstract public class Credential<T> {
    private VCMetadata meta;

    public Credential() {}

    public Credential(@Nonnull VCMetadata meta) {
        this.meta = meta;
    }

    public VCMetadata getMeta() {
        return this.meta;
    }

    public void setMeta(@Nonnull VCMetadata meta) {
        this.meta = meta;
    }

    abstract public int getClaimCount();

    abstract public T getCredentialSubject();
}
