package com.rep.vc.models.vp;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.rep.vc.models.vc.VerifiableCredential;

import java.io.IOException;
import java.util.List;

public class VerifiablePresentationSerializer extends StdSerializer<VerifiablePresentation> {

    public VerifiablePresentationSerializer() {
        this(null);
    }

    public VerifiablePresentationSerializer(Class<VerifiablePresentation> t) {
        super(t);
    }

    @Override
    public void serialize(VerifiablePresentation value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        VPMetadata metadata = value.getMetadata();
        List<VerifiableCredential> verfiableCredentialList =
                value.getVerifiableCredential();
        VPProof proof = value.getProof();

        String[] context = new String[metadata.getContext().size()];
        metadata.getContext().toArray(context);
        String[] type = new String[metadata.getType().size()];
        metadata.getType().toArray(type);
        gen.writeStartObject();
        gen.writeFieldName("@context");
        gen.writeArray(context, 0, context.length);
        gen.writeStringField("id", metadata.getId());
        gen.writeFieldName("type");
        gen.writeArray(type, 0, type.length);
        gen.writeStringField("holder", metadata.getHolder());
        gen.writeFieldName("verifiableCredential");
        if (verfiableCredentialList.size() > 1) {
            gen.writeStartArray();
            for (VerifiableCredential vc : verfiableCredentialList) {
                gen.writeObject(vc);
            }
            gen.writeEndArray();
        } else {
            gen.writeObject(verfiableCredentialList.get(0));
        }
        gen.writeFieldName("proof");
        gen.writeStartObject();
        gen.writeStringField("type", proof.getType());
        gen.writeStringField("created", proof.getCreated());
        gen.writeStringField("verificationMethod", proof.getVerificationMethod());
        gen.writeStringField("challenge", proof.getChallenge());
        gen.writeStringField("signature", proof.getSignature());
        gen.writeEndObject();
        gen.writeEndObject();
    }
}
