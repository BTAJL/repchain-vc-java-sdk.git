package com.rep.vc.models.vp;

import com.rep.vc.models.vc.VCProof;

import javax.annotation.Nonnull;

/**
 * 可验证凭据出示信息的证明信息，证明信息中包含了签名信息
 *
 * @author jayTsang
 */
public class VPProof extends VCProof {
    private String challenge;
    private String signature ;

    /**
     * VPProof构造方法
     * @param type 签名算法，目前支持EcdsaSecp256k1Signature/EcdsaSecp256k1Signature2019/EcdsaPrime256v1Signature/RsaSignature2018
     * @param created 签名时间
     * @param verificationMethod 验签公钥标识，通过did uri表示的公钥标识
     * @param challenge 签名挑战信息
     * @param signature 签名数据，Base64编码的字符串
     */
    public VPProof(
            @Nonnull String type,
            @Nonnull String created,
            @Nonnull String verificationMethod,
            @Nonnull String challenge,
            @Nonnull String signature
    ) {
        super(type, created, verificationMethod);
        this.challenge = challenge;
        this.signature = signature;
    }

    public String getChallenge() {
        return challenge;
    }

    @Override
    public String getSignature() {
        return signature;
    }
}
