package com.rep.vc.models.vp;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.rep.vc.models.vc.VerifiableCredential;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class VerifiablePresentationDeserializer extends StdDeserializer<VerifiablePresentation> {
    public VerifiablePresentationDeserializer() {
        this(null);
    }

    public VerifiablePresentationDeserializer(Class<VerifiablePresentation> t) {
        super(t);
    }

    @Override
    public VerifiablePresentation deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = p.readValueAsTree();

        VPMetadata metadata = new VPMetadata();
        JsonNode contextNode = node.get("@context");
        List<String> context = IntStream.range(0, contextNode.size()).boxed().map(
                i -> contextNode.get(i).asText()
        ).collect(Collectors.toList());
        metadata.setContext(context);
        metadata.setId(node.get("id").asText());
        JsonNode typeNode = node.get("type");
        List<String> type = IntStream.range(0, typeNode.size()).boxed().map(
                i -> typeNode.get(i).asText()
        ).collect(Collectors.toList());
        metadata.setType(type);
        metadata.setHolder(node.get("holder").asText());

        List<VerifiableCredential> vcList = new ArrayList<>();
        JsonNode verifiableCredentialNode = node.get("verifiableCredential");
        if (verifiableCredentialNode.isArray()) {
            for (int i = 0; i < verifiableCredentialNode.size(); i++) {
                VerifiableCredential vc = VerifiableCredential.parseFrom(verifiableCredentialNode.get(i).toString());
                vcList.add(vc);
            }
        } else {
            VerifiableCredential vc = VerifiableCredential.parseFrom(verifiableCredentialNode.toString());
            vcList.add(vc);
        }

        JsonNode proofNode = node.get("proof");
        VPProof proof = new VPProof(
                proofNode.get("type").asText(),
                proofNode.get("created").asText(),
                proofNode.get("verificationMethod").asText(),
                proofNode.get("challenge").asText(),
                proofNode.get("signature").asText()
        );

        VerifiablePresentation vp = new VerifiablePresentation(metadata, vcList, proof);
        return vp;
    }
}
