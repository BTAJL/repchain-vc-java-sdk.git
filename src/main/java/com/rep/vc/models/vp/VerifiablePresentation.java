package com.rep.vc.models.vp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.protobuf.InvalidProtocolBufferException;
import com.rep.vc.models.proto.VCProto;
import com.rep.vc.models.vc.VerifiableCredential;
import com.rep.vc.utils.Config;
import com.rep.vc.utils.MyValidations;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.*;
import java.util.ArrayList;
import java.util.List;

import static com.rep.vc.utils.Misc.getCurrentISODateTime;
import static com.rep.vc.utils.SignatureTools.*;

/**
 * 可验证凭据出示信息VerifiablePresentation，
 * 主要包含元数据metadata、可验证凭据verifiableCredential和证明信息Proof，
 * 其中可验证凭据可为单个或多个。
 * 此外还含有属性使用Protobuf定义的可验证凭据出示pvp。
 * 属性pvp和本类实例对象互为镜像，
 * 其中属性pvp主要用于二进制格式可验证凭据出示信息的序列化和反序列化，
 * 以及签名验签过程中待签名数据TBS的生成；
 * 而本类实例对象主要用于json格式可验证凭据出示信息的序列化和反序列化
 *
 * @author jayTsang
 */
@JsonSerialize(using = VerifiablePresentationSerializer.class)
@JsonDeserialize(using = VerifiablePresentationDeserializer.class)
public class VerifiablePresentation {
    VPMetadata metadata;
    List<VerifiableCredential> verifiableCredential;
    VPProof proof;
    // VCPresentation in proto format
    VCProto.VerifiablePresentation pvp;

    public VerifiablePresentation() {}

    /**
     * VerifiablePresentation构造方法，
     * 可验证凭据出示信息初始时没有被签名，在被签名时自动生成证明信息proof
     * @param metadata 可验证凭据出示信息元数据对象
     * @param verifiableCredential 可验证凭据对象构成的List对象
     */
    public VerifiablePresentation(
            @Nonnull VPMetadata metadata,
            @Nonnull List<VerifiableCredential> verifiableCredential
    ) {
        this.metadata = metadata;
        this.verifiableCredential = verifiableCredential;
        // create VerifiablePresentation proto message without proof
        this.pvp = generateVPProtoBuilder(metadata, verifiableCredential).build();
    }


    /**
     * VerifiablePresentation构造方法，用于从Json字符串反序列化，
     * 使用者应使用其他构造方法
     * @param metadata 可验证凭据出示信息元数据对象
     * @param verifiableCredential 可验证凭据对象构成的List对象
     * @param proof 凭据出示证明信息对象
     */
    public VerifiablePresentation(
            @Nonnull VPMetadata metadata,
            @Nonnull List<VerifiableCredential> verifiableCredential,
            @Nonnull VPProof proof
    ) {
        this.metadata = metadata;
        this.verifiableCredential = verifiableCredential;
        this.proof = proof;
        VCProto.VerifiablePresentation.Builder vpb = this.generateVPProtoBuilder(metadata, verifiableCredential);
        VCProto.VerifiablePresentation.VPProof.Builder proofBuilder =
                VCProto.VerifiablePresentation.VPProof.newBuilder();
        proofBuilder.setType(proof.getType())
                .setCreated(proof.getCreated())
                .setVerificationMethod(proof.getVerificationMethod())
                .setChallenge(proof.getChallenge())
                .setSignature(proof.getSignature());
        vpb.setProof(proofBuilder.build());
        this.pvp = vpb.build();
    }

    /**
     * 生成可验证凭据出示Protobuf Builder
     * @param metadata 可验证凭据出示元数据对象
     * @param verifiableCredential 可验证凭据构成的List对象
     * @return
     */
    private VCProto.VerifiablePresentation.Builder generateVPProtoBuilder (
            VPMetadata metadata, List<VerifiableCredential> verifiableCredential
    ) {
        VCProto.VerifiablePresentation.Builder vpb =
                VCProto.VerifiablePresentation.newBuilder();
        vpb.setVersion(Config.getInstance().getVCProtoVersion())
                .addAllContext(metadata.getContext())
                .setId(metadata.getId())
                .addAllType(metadata.getType())
                .setHolder(metadata.getHolder());
        List<VCProto.VerifiableCredential> protoVerifiableCredentialList =
                new ArrayList<>();
        for (VerifiableCredential element : verifiableCredential) {
            protoVerifiableCredentialList.add(element.getPvc());
        }
        vpb.addAllVerifiableCredential(protoVerifiableCredentialList);
        return vpb;
    }

    /**
     * 对凭据出示信息进行签名
     * @param prvKey 签名所用私钥对象
     * @param pubKeyId 验证签名时应使用的公钥的标识
     * @param sigAlg 签名算法，目前支持EcdsaSecp256k1Signature/EcdsaSecp256k1Signature2019/EcdsaPrime256v1Signature/RsaSignature2018
     * @param challenge 签名时加入的挑战信息，用于验证签名时防重放攻击
     * @throws Exception
     */
    public void sign(
            @Nonnull PrivateKey prvKey,
            @Nonnull String pubKeyId,
            @Nonnull String sigAlg,
            @Nonnull String challenge
    ) throws Exception {
        if (this.proof != null) {
            throw new Exception("不能对已被签名的可验证凭据出示信息签名");
        }
        MyValidations.isNotEmpty("pubKeyId", pubKeyId);
        MyValidations.isNotEmpty("sigAlg", sigAlg);

        VCProto.VerifiablePresentation.Builder vpb =
                VCProto.VerifiablePresentation.newBuilder();
        String alg = convertSigAlg(sigAlg);
        VCProto.VerifiablePresentation.VPProof.Builder pb =
                VCProto.VerifiablePresentation.VPProof.newBuilder();
        String created = getCurrentISODateTime();
        pb.setType(sigAlg)
                .setCreated(created)
                .setVerificationMethod(pubKeyId)
                .setChallenge(challenge);
        vpb.mergeFrom(this.pvp).setProof(pb.build());
        byte[] tbs = generateVerifiablePresentationTbs(vpb.build());
        String signature = sign2Base64(alg, prvKey, tbs);

        this.proof = new VPProof(sigAlg, created, pubKeyId, challenge, signature);

        vpb.getProofBuilder().setSignature(signature);
        this.pvp = vpb.build();
    }

    /**
     * 验证可验证凭据出示信息中的签名信息，
     * 将自动从RepChain获取相应验签公钥信息
     * @return 通过验证则返回true，否则返回false
     * @throws IOException
     */
    public boolean verifySignature() throws IOException {
        if (this.proof != null) {
            return verifyVerifiablePresentation(this);
        }
        return false;
    }

    /**
     * 使用给定的公钥信息对验证可验证凭据出示信息中的签名信息进行验证，
     * @param pubKey 用来验签的公钥对象
     * @return 通过验证则返回true，否则返回false
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public boolean verifySignature(@Nonnull PublicKey pubKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        if (this.proof != null) {
            return verifyVerifiablePresentation(this, pubKey);
        }
        return false;
    }

    /**
     * 将可验证凭据出示对象导出为二进制格式
     * @return 二进制格式的可验证凭据出示信息
     */
    public byte[] exportAsBytes()  {
        return this.pvp.toByteArray();
    }

    /**
     * 将可验证凭据出示对象导出为Json字符串
     * @return Json字符串格式的可验证凭据出示信息
     * @throws JsonProcessingException
     */
    public String exportAsJsonStr() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    /**
     * 将二进制数据解析为可验证凭据出示信息对象
     * @param vpProtoBytes 二进制格式的可验证凭据出示
     * @return
     */
    public static VerifiablePresentation parseFrom(byte[] vpProtoBytes) {
        try {
            VCProto.VerifiablePresentation message =
                    VCProto.VerifiablePresentation.parseFrom(vpProtoBytes);
            return parseFrom(message);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将Protobuf对象解析为可验证凭据出示信息对象
     * @param message 可验证凭据出示信息的protobuf对象
     * @return
     */
    public static VerifiablePresentation parseFrom(VCProto.VerifiablePresentation message) {
        VerifiablePresentation verifiablePresentation = new VerifiablePresentation();
        verifiablePresentation.setPvp(message);
        VPMetadata metadata = new VPMetadata();
        metadata.setContext(message.getContextList());
        metadata.setHolder(message.getHolder());
        metadata.setId(message.getId());
        metadata.setType(message.getTypeList());
        verifiablePresentation.setMetadata(metadata);
        List<VerifiableCredential> vcList = new ArrayList<>();
        for (VCProto.VerifiableCredential vc : message.getVerifiableCredentialList()) {
            vcList.add(VerifiableCredential.parseFrom(vc));
        }

        verifiablePresentation.setVerifiableCredential(vcList);
        VCProto.VerifiablePresentation.VPProof vpProof = message.getProof();
        VPProof proof = new VPProof(
                vpProof.getType(),
                vpProof.getCreated(),
                vpProof.getVerificationMethod(),
                vpProof.getChallenge(),
                vpProof.getSignature()
        );
        verifiablePresentation.setProof(proof);
        return verifiablePresentation;
    }

    /**
     * 将Json字符串解析为可验证凭据出示信息对象
     * @param vpJsonStr Json字符串格式的可验证凭据出示信息
     * @return
     * @throws JsonProcessingException
     */
    public static VerifiablePresentation parseFrom(String vpJsonStr) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(vpJsonStr, VerifiablePresentation.class);
    }

    public VPMetadata getMetadata() {
        return metadata;
    }

    private void setMetadata(VPMetadata metadata) {
        this.metadata = metadata;
    }

    public List<VerifiableCredential> getVerifiableCredential() {
        return verifiableCredential;
    }

    private void setVerifiableCredential(List<VerifiableCredential> verifiableCredential) {
        this.verifiableCredential = verifiableCredential;
    }

    public VPProof getProof() {
        return proof;
    }

    private void setProof(VPProof proof) {
        this.proof = proof;
    }

    public VCProto.VerifiablePresentation getPvp() {
        return pvp;
    }

    private void setPvp(VCProto.VerifiablePresentation pvp) {
        this.pvp = pvp;
    }
}
