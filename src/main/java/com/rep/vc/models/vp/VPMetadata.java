package com.rep.vc.models.vp;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 可验证凭据出示信息的元数据
 *
 * @author jayTsang
 */
public class VPMetadata {
    /** 对应ｗ3c vc-data-model标准中的＠context */
    private List<String> context =
            new ArrayList<>(Arrays.asList(new String[]{"https://www.w3.org/2018/credentials/v1"}));
    /** 可验证凭据出示唯一标识 */
    private String id;
    /** 可验证凭据出示类型 */
    private List<String> type =
            new ArrayList<>(Arrays.asList(new String[]{"VerifiablePresentation"}));
    /** 持有者的did标识 */
    private String holder;

    public VPMetadata() {}

    public List<String> getContext() {
        return context;
    }

    /**
     * 设置context的值，根据ｗ3c vc-data-model标准，
     * 其第一个元素应固定为: "https://www.w3.org/2018/credentials/v1"
     * @param context
     */
    public void setContext(@Nonnull List<String> context) {
        this.context = context;
    }

    public void addContext(@Nonnull String context)  {
        this.context.add(context);
    }

    public void addContext(@Nonnull int index, @Nonnull String context)  {
        this.context.add(index, context);
    }

    public String getId() {
        return id;
    }

    public void setId(@Nonnull String id) {
        this.id = id;
    }

    public List<String> getType() {
        return type;
    }

    /**
     * 设置type的值，根据ｗ3c vc-data-model标准，
     * 其第一个元素应固定为: "VerifiablePresentation"
     * @param type
     */
    public void setType(@Nonnull List<String> type) {
        this.type = type;
    }

    public void addType(@Nonnull String type) {
        this.type.add(type);
    }

    public void addType(@Nonnull int index, @Nonnull String type) {
        this.type.add(index, type);
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(@Nonnull String holder) {
        this.holder = holder;
    }
}
