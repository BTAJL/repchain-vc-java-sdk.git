package com.rep.vc.models.vcstatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rep.vc.utils.Config;
import com.rep.vc.utils.HttpClient;
import com.rep.vc.utils.RepChainResponse;

import javax.annotation.Nonnull;

/**
 * 可验证凭据状态
 *
 * @author jayTsang
 */
public class VCStatus {
    private String id;
    private String status;
    private String[] revokedClaimIndex;
    private String creator;

    public VCStatus() {}

    /**
     * 从RepChain网络中获取可验证凭据状态信息VCStatus，
     * @param id 可验证凭据的Id
     * @return 若获取到相应id的VCStatus则将其返回，否则返回null
     */
    static public VCStatus getVCStatusFromRepChain(@Nonnull String id) {
        String stateKey = Config.getInstance().getVCVCStatusWorldStatePrefix() + id;

        try {
            HttpClient client = new HttpClient();
            String vcStatusJsonStr = client.queryWorldstate(
                    Config.getInstance().getNetworkId(),
                    Config.getInstance().getOId(),
                    Config.getInstance().getVCConstractName(),
                    stateKey
            );
            ObjectMapper om = new ObjectMapper();
            om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            RepChainResponse<VCStatus> resVCStatus = om.readValue(
                    vcStatusJsonStr,
                    new TypeReference<RepChainResponse<VCStatus>>() {}
            );
            return resVCStatus.result;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String[] getRevokedClaimIndex() {
        return revokedClaimIndex;
    }

    public void setRevokedClaimIndex(String[] revokedClaimIndex) {
        this.revokedClaimIndex = revokedClaimIndex;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
