package com.rep.vc.models.ccs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rep.vc.utils.*;

import javax.annotation.Nonnull;

/**
 * 可验证凭据属性结构CCS
 *
 * @author jayTsang
 */
public class CredentialClaimStruct {
    private String id;
    private String name;
    private String version;
    private String description;
    private String created;
    private CredentialClaimStructAttr[] attributes;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String creator;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Boolean valid;

    // For json deserialization
    public CredentialClaimStruct() {
        this.created = Misc.getCurrentISODateTime();
    }

    /**
     * ＣredentialClaimStruct构造方法
     * @param id 可验证凭据属性结构唯一标识
     * @param name 可验证凭据属性结构名称
     * @param version 可验证凭据属性结构版本
     * @param description 可验证凭据属性结构描述信息
     * @param attributes 可验证凭据属性结构属性信息构成的数组
     */
    public CredentialClaimStruct(
        String id,
        String name,
        String version,
        String description,
        CredentialClaimStructAttr[] attributes
    ) {
        MyValidations.isNotEmpty("id", id);
        MyValidations.isNotEmpty("name", name);
        MyValidations.isNotEmpty("version", version);
        MyValidations.isNotEmpty("description", description);
        MyValidations.isNotEmpty("attributes", attributes);
        this.id = id;
        this.name = name;
        this.version = version;
        this.created = Misc.getCurrentISODateTime();
        this.description = description;
        this.attributes = attributes;
    }

     /**
      * 从RepChain网络中获取可验证凭据属性结构CCS信息，
      * @param id 可验证凭据属性结构的Id
      * @return 若获取到相应id的可验证凭据属性结构信息则将其返回，否则返回null
      */
     static public CredentialClaimStruct getCCSFromRepChain(@Nonnull String id) {
         String stateKey = Config.getInstance().getVCCCSWorldStatePrefix() + id;

         try {
             HttpClient client = new HttpClient();
             String ccsJsonStr = client.queryWorldstate(
                     Config.getInstance().getNetworkId(),
                     Config.getInstance().getOId(),
                     Config.getInstance().getVCConstractName(),
                     stateKey
             );
             ccsJsonStr = ccsJsonStr.replace("version", "ccsProtoBufMsgVersion");
             ccsJsonStr = ccsJsonStr.replace("ccsVersion", "version");
             ObjectMapper om = new ObjectMapper();
             om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
             RepChainResponse<CredentialClaimStruct> resCCS = om.readValue(
                     ccsJsonStr,
                     new TypeReference<RepChainResponse<CredentialClaimStruct>>() {}
             );
             return resCCS.result;
         } catch (JsonProcessingException e) {
             e.printStackTrace();
         }
         return null;
     }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public String getCreated() {
        return created;
    }

    public String getDescription() {
        return description;
    }

    public String getCreator() {
        return creator;
    }

    public Boolean getValid() {
        return valid;
    }

    public CredentialClaimStructAttr[] getAttributes() {
        return attributes;
    }
}
