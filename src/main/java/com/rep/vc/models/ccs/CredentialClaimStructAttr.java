package com.rep.vc.models.ccs;

import com.rep.vc.utils.MyValidations;

/**
 * 可验证凭据属性结构CCS中的具体属性信息
 *
 * @author jayTsang
 */
public class CredentialClaimStructAttr {
    private String name;
    private String type;
    private Boolean required;
    private String description;

    // For json deserialization
    public CredentialClaimStructAttr() {}

    /**
     * CredentialClaimStructAttr构造方法
     * @param name 属性名称
     * @param type 属性值的类型，属性值使用字符串表示，因此使用者应根据属性值类型去解析属性值
     * @param required 属性是否在可验证凭据中必须出现
     * @param description 属性描述
     */
    public CredentialClaimStructAttr(
        String name,
        String type,
        Boolean required,
        String description
    ) {
        MyValidations.isNotEmpty("name", name);
        MyValidations.isNotEmpty("type", type);
        MyValidations.isNotEmpty("description", description);
        MyValidations.isNotNull("required", required);
        this.name = name;
        this.type = type;
        this.required = required;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Boolean getRequired() {
        return required;
    }

    public String getDescription() {
        return description;
    }
}
