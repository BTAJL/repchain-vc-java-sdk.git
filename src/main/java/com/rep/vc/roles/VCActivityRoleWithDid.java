package com.rep.vc.roles;

import com.rep.vc.utils.MyValidations;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.PrivateKey;

public class VCActivityRoleWithDid extends VCActivityRole {
    String did;

    public VCActivityRoleWithDid(String did, String certName, PrivateKey prvKey) {
        super(getAccountId(did), certName, prvKey);
        this.did = did;
    }

    public VCActivityRoleWithDid(String did, String certName, String prvKeyPemStr) throws IOException {
        super(getAccountId(did), certName, prvKeyPemStr);
        this.did = did;
    }

    static private String getAccountId(@Nonnull String did) {
        MyValidations.isValidDid(did);
        String[] parts = did.split(":");
        return parts[2] + ":" + parts[parts.length - 1];
    }

    public String getPublicKeyId() {
        return this.did + "#" + this.getCertName();
    }

    public String getDid() {
        return did;
    }
}
