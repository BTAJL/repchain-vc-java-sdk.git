package com.rep.vc.roles;

import java.io.IOException;
import java.security.PrivateKey;

import com.rep.vc.utils.SignatureTools;
import com.rep.vc.utils.MyValidations;

import javax.annotation.Nonnull;

public abstract class VCActivityRole {
    private String accountId;

    private String certName;

    private PrivateKey prvKey;

    public VCActivityRole(
            @Nonnull String accountId,
            @Nonnull String certName,
            @Nonnull PrivateKey prvKey
    ) {
        MyValidations.isNotEmpty("accountId", accountId);
        MyValidations.isNotEmpty("certName", certName);
        setAccountId(accountId);
        setCertName(certName);
        setPrvKey(prvKey);
    }

    public VCActivityRole(
            @Nonnull String accountId,
            @Nonnull String certName,
            @Nonnull String prvKeyPemStr
    ) throws IOException {
        MyValidations.isNotEmpty("accountId", accountId);
        MyValidations.isNotEmpty("certName", certName);
        MyValidations.isNotEmpty("prvKeyPemStr", prvKeyPemStr);
        setAccountId(accountId);
        setCertName(certName);
        setPrvKey(prvKeyPemStr);
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        MyValidations.isNotEmpty("accountId", accountId);
        this.accountId = accountId;
    }

    public String getCertName() {
        return certName;
    }

    public void setCertName(String certName) {
        MyValidations.isNotEmpty("certName", certName);
        this.certName = certName;
    }

    public PrivateKey getPrvKey() {
        return prvKey;
    }

    public void setPrvKey(@Nonnull PrivateKey prvKey) {
        this.prvKey = prvKey;
    }

    public void setPrvKey(String prvKeyPemStr) throws IOException {
        MyValidations.isNotEmpty("prvKeyPemStr", prvKeyPemStr);
        PrivateKey prvKey = SignatureTools.getPrvKey(prvKeyPemStr);
        this.prvKey = prvKey;
    }
}
