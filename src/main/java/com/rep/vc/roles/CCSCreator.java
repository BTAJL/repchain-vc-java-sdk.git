package com.rep.vc.roles;

import com.rcjava.client.TranPostClient;
import com.rep.vc.models.ccs.CredentialClaimStruct;
import com.rep.vc.utils.Config;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rep.vc.utils.Transaction;
import com.rep.vc.utils.MyValidations;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.PrivateKey;

/**
 * 可验证凭据属性结构创建者，主要负责向RepChain注册可验证凭据属性结构
 *
 * @author jayTsang
 */
public class CCSCreator extends VCActivityRoleWithDid  {

    /**
     * CCSCreator构造方法
     * @param did 可验证凭据属性结构创建者的did标识
     * @param certName 可验证凭据属性结构创建者使用的证书名称(即公钥名称)
     * @param prvKey 可验证凭据属性结构创建者使用的私钥对象
     */
    public CCSCreator(
            String did,
            String certName,
            PrivateKey prvKey
    ) {
        super(did, certName, prvKey);
    }

    /**
     * CCSCreator构造方法
     * @param did 可验证凭据属性结构创建者的did标识
     * @param certName 可验证凭据属性结构创建者使用的证书名称
     * @param prvKeyPemStr 可验证凭据属性结构创建者使用的pem格式的私钥
     * @throws IOException
     */
    public CCSCreator(
            String did,
            String certName,
            String prvKeyPemStr
    ) throws IOException {
        super(did, certName, prvKeyPemStr);
    }

    /**
     * 通过签名交易，向RepChain网络注册可验证凭据属性结构CCS
     * @param ccs 欲被注册的表示可验证凭据属性结构的CredentialClaimStruct对象
     * @return RepChain节点返回的结果Json字符串
     * @throws Exception
     */
    public String signupCCS2RepChain(@Nonnull CredentialClaimStruct ccs) throws Exception {
        String ccsJsonStr = new ObjectMapper().writeValueAsString(ccs);
        TranPostClient tpc = new TranPostClient(Config.getInstance().getPeerAddress());
        Transaction.Contract contract = new Transaction.Contract(
                Config.getInstance().getVCConstractName(),
                Config.getInstance().getVCConstractVersion(),
                Config.getInstance().getOId(),
                Config.getInstance().getVCSignupCCSFunctionName(),
                ccsJsonStr
        );
        return Transaction.postTx(this, tpc, contract);
    }

    /**
     * 在RepChain网络中更新可验证凭据属性结构ccs的有效性状态
     * @param id ccs的Id
     * @param valid ccs的有效性
     * @return RepChain节点返回的结果Json字符串
     * @throws Exception
     */
    public String updateCCSStatus(String id, Boolean valid) throws Exception {
        MyValidations.isNotEmpty("id", id);

        CCSStatus ccsStatus = new CCSStatus(id, valid);
        String ccsStatusJsonStr = new ObjectMapper().writeValueAsString(ccsStatus);
        TranPostClient tpc = new TranPostClient(Config.getInstance().getPeerAddress());
        Transaction.Contract contract = new Transaction.Contract(
                Config.getInstance().getVCConstractName(),
                Config.getInstance().getVCConstractVersion(),
                Config.getInstance().getOId(),
                Config.getInstance().getVCUpdateCCSStatusFunctionName(),
                ccsStatusJsonStr
        );
        return Transaction.postTx(this, tpc, contract);
    }
}

class CCSStatus {
    private String id;
    private Boolean valid;

    public CCSStatus(String id, Boolean valid) {
        this.id = id;
        this.valid = valid;
    }

    public String getId() {
        return id;
    }

    public Boolean getValid() {
        return valid;
    }
}