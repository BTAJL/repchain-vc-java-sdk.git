package com.rep.vc.roles;

import com.rep.vc.models.vc.VerifiableCredential;
import com.rep.vc.models.vp.VerifiablePresentation;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SignatureException;

/**
 * 可验证凭据验证者
 *
 * @author jayTsang
 */
public class VCVerifier extends VCActivityRoleWithDid {

    /**
     * VCVerifier构造方法
     * @param did 可验证凭据验证者的did标识
     * @param certName 可验证凭据验证者使用的证书名称(即公钥名称)
     * @param prvKey 可验证凭据验证者使用的私钥对象
     */
    public VCVerifier(String did, String certName, PrivateKey prvKey) {
        super(did, certName, prvKey);
    }

    /**
     * VCVerifier构造方法
     * @param did 可验证凭据验证者的did标识
     * @param certName 可验证凭据验证者使用的证书名称(即公钥名称)
     * @param prvKeyPemStr 可验证凭据验证者使用的pem格式私钥
     * @throws IOException
     */
    public VCVerifier(String did, String certName, String prvKeyPemStr) throws IOException {
        super(did, certName, prvKeyPemStr);
    }

    /**
     * 验证可验证凭据出示信息的正确性，将对以下内容进行验证：
     * - 可验证凭据出示信息包含的挑战信息应与目标挑战信息相符
     * - 可验证凭据出示信息包含的每个可验证凭据应处于有效期限内
     * - 可验证凭据出示信息包含的每个可验证凭据应处于期望状态，且每个属性处于有效状态
     * - 可验证凭据出示信息包含的每个可验证凭据的签名应正确
     * - 可验证凭据出示信息的签名应正确
     * @param vp 可验证凭据出示对象
     * @param targetChallenge 目标挑战信息
     * @param targetStatus 期望可验证凭据处于的目标状态数组
     * @return 返回表示验证结果的bool值，true表示验证通过，false表示验证失败
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    static public boolean verify(VerifiablePresentation vp, String targetChallenge, String[] targetStatus) throws IOException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        // the chellenge must be matched
        if (!vp.getProof().getChallenge().equals(targetChallenge)) {
            return false;
        }

        // verify each VerifiableCrederntial
        for (int i = 0;  i < vp.getVerifiableCredential().size(); i++) {
            VerifiableCredential vc = vp.getVerifiableCredential().get(i);
            if (!vc.verifyInValidPeriod()) {
                return false;
            }
            if (!vc.verifyStatusAndClaims(targetStatus[i])) {
                return false;
            }
            if (!vc.verifySignature()) {
                return false;
            }
        }

        // the signatures must be matched
        return vp.verifySignature();
    }
}
