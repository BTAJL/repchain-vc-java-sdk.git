package com.rep.vc.roles;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rcjava.client.TranPostClient;
import com.rep.vc.models.vc.*;
import com.rep.vc.utils.*;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.PrivateKey;

import static com.rep.vc.utils.Misc.getCurrentISODateTime;

/**
 * 可验证凭据签发者，主要负责签发可验证凭据VC，
 * 在RepChain中初始化或更新可验证凭据状态
 *
 * @author jayTsang
 */
public class VCIssuer extends VCActivityRoleWithDid {
    /**
     * VCIssuer构造方法
     * @param did 可验证凭据签发者的did标识
     * @param certName 可验证凭据签发者使用的证书名称(即公钥名称)
     * @param prvKey 可验证凭据签发者使用的私钥对象
     */
    public VCIssuer(
            String did,
            String certName,
            PrivateKey prvKey
    ) {
        super(did, certName, prvKey);
    }

    /**
     * VCIssuer构造方法
     * @param did 可验证凭据签发者的did标识
     * @param certName 可验证凭据签发者使用的证书名称(即公钥名称)
     * @param prvKeyPemStr 可验证凭据签发者使用的pem格式的私钥
     * @throws IOException
     */
    public VCIssuer(
            String did,
            String certName,
            String prvKeyPemStr
    ) throws IOException {
        super(did, certName, prvKeyPemStr);
    }

    /**
     * 使用原子性签名模式签发可验证凭据VC
     * @param credential Credential凭据对象，包含元数据metadata和凭据描述对象属性credentialSubject
     * @param sigAlg 签名算法，目前支持EcdsaSecp256k1Signature/EcdsaSecp256k1Signature2019/EcdsaPrime256v1Signature/RsaSignature2018
     * @return 返回可验证凭据VerifiableCredential对象
     * @throws Exception
     */
    public VerifiableCredential issueVerifiableCredential(
            @Nonnull Credential credential,
            @Nonnull String sigAlg
    ) throws Exception {
        // 使用Issuer的did设置/覆盖可验证凭据metadata中的issuer属性值
        credential.getMeta().setIssuer(this.getDid());
        // 使用当前机器时间设置/覆盖可验证凭据metadata中的issued属性值
        String issued = getCurrentISODateTime();
        credential.getMeta().setIssued(issued);
        VerifiableCredential vc = new VerifiableCredential(credential);
        // TODO: signup VCStatus ?
        vc.sign(
                this.getPrvKey(),
                this.getPublicKeyId(),
                sigAlg
        );
        return vc;
    }

    /**
     * 签发可验证凭据VC
     * @param credential Credential凭据对象，包含元数据metadata和凭据描述对象属性credentialSubject
     * @param sigAlg 签名算法，目前支持EcdsaSecp256k1Signature/EcdsaSecp256k1Signature2019/EcdsaPrime256v1Signature/RsaSignature2018
     * @param sigScheme 签名模式，目前支持automic_sig_scheme(原子模式)/merkle_sig_scheme(merkle模式)
     * @return 返回可验证凭据VerifiableCredential对象
     * @throws Exception
     */
    public VerifiableCredential issueVerifiableCredential(
            @Nonnull Credential credential,
            @Nonnull String sigAlg,
            @Nonnull String sigScheme
    ) throws Exception {
        credential.getMeta().setIssuer(this.getDid());
        String issued = getCurrentISODateTime();
        credential.getMeta().setIssued(issued);
        VerifiableCredential vc = new VerifiableCredential(credential);
        vc.sign(
                this.getPrvKey(),
                this.getPublicKeyId(),
                sigAlg,
                sigScheme
        );
        return vc;
    }

    /**
     * 通过签名交易，向RepChain注册可验证凭据初始状态
     * @param id 可验证凭据的唯一标识
     * @param status 可验证凭据的初始状态
     * @return RepChain节点返回的结果Json字符串
     * @throws Exception
     */
    public String signupVCStatus(String id, String status) throws Exception {
        VCStatusParam vcStatus = new VCStatusParam(id, status);
        String vcStatusJsonStr = new ObjectMapper().writeValueAsString(vcStatus);
        TranPostClient tpc = new TranPostClient(Config.getInstance().getPeerAddress());
        Transaction.Contract contract = new Transaction.Contract(
                Config.getInstance().getVCConstractName(),
                Config.getInstance().getVCConstractVersion(),
                Config.getInstance().getOId(),
                Config.getInstance().getVCSignupVCStatusFunctionName(),
                vcStatusJsonStr
        );
        return Transaction.postTx(this, tpc, contract);
    }

    /**
     * 通过签名交易，向RepChain更新可验证凭据的状态
     * @param id 可验证凭据的唯一标识
     * @param status 可验证凭据更新后的状态
     * @return RepChain节点返回的结果Json字符串
     * @throws Exception
     */
    public String updateVCStatus(String id, String status) throws Exception {
        VCStatusParam vcStatus = new VCStatusParam(id, status);
        String vcStatusJsonStr = new ObjectMapper().writeValueAsString(vcStatus);
        TranPostClient tpc = new TranPostClient(Config.getInstance().getPeerAddress());
        Transaction.Contract contract = new Transaction.Contract(
                Config.getInstance().getVCConstractName(),
                Config.getInstance().getVCConstractVersion(),
                Config.getInstance().getOId(),
                Config.getInstance().getVCUpdateVCStatusFunctionName(),
                vcStatusJsonStr
        );
        return Transaction.postTx(this, tpc, contract);
    }

    /**
     * 通过签名交易，撤销可验证凭据属性，当只需要更新可验证凭据的部分属性(credentialSubject的内容)时可使用该方法
     * @param id 可验证凭据唯一标识
     * @param revokedClaimIndex 欲撤销的可验证凭据属性的编号数组
     * @return RepChain节点返回的结果Json字符串
     * @throws Exception
     */
    public String revokeVCClaims(String id, String[] revokedClaimIndex) throws Exception {
        VCRevokeClaim vcRevokeClaim = new VCRevokeClaim(id, revokedClaimIndex);
        String vcRevokeClaimJsonStr = new ObjectMapper().writeValueAsString(vcRevokeClaim);
        TranPostClient tpc = new TranPostClient(Config.getInstance().getPeerAddress());
        Transaction.Contract contract = new Transaction.Contract(
                Config.getInstance().getVCConstractName(),
                Config.getInstance().getVCConstractVersion(),
                Config.getInstance().getOId(),
                Config.getInstance().getVCRevokeVCClaimsFunctionName(),
                vcRevokeClaimJsonStr
        );
        return Transaction.postTx(this, tpc, contract);
    }
}

class VCStatusParam {
    private String id;
    private String status;

    public VCStatusParam(@Nonnull String id, @Nonnull String status) {
        this.id = id;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }
}

class VCRevokeClaim {
    private String id;
    private String[] revokedClaimIndex;

    public VCRevokeClaim(@Nonnull String id, @Nonnull String[] claimIndex) {
        this.id = id;
        this.revokedClaimIndex = claimIndex;
    }

    public String getId() {
        return id;
    }

    public String[] getRevokedClaimIndex() {
        return revokedClaimIndex;
    }
}
