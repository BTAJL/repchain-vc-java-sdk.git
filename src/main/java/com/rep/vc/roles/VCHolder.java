package com.rep.vc.roles;

import com.rep.vc.models.proto.VCProto;
import com.rep.vc.models.vc.CredentialAtomic;
import com.rep.vc.models.vc.VerifiableCredential;
import com.rep.vc.models.vp.VPMetadata;
import com.rep.vc.models.vp.VerifiablePresentation;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 可验证凭据持有者，负责选择性披露可验证凭据及生成可验证凭据出示信息
 *
 * @author jayTsang
 */
public class VCHolder extends VCActivityRoleWithDid {

    /**
     * VCHolder构造方法
     * @param did 可验证凭据持有者的did标识
     * @param certName 可验证持有者使用的证书名称(即公钥名称)
     * @param prvKey 可验证持有者使用的私钥对象
     */
    public VCHolder(String did, String certName, PrivateKey prvKey) {
        super(did, certName, prvKey);
    }

    /**
     * VCHolder构造方法
     * @param did 可验证凭据持有者的did标识
     * @param certName 可验证持有者使用的证书名称(即公钥名称)
     * @param prvKeyPemStr 可验证持有者使用的pem格式私钥
     */
    public VCHolder(String did, String certName, String prvKeyPemStr) throws IOException {
        super(did, certName, prvKeyPemStr);
    }

    /**
     * 根据claim编号选择性披露可验证凭据(基于原子性签名的可验证凭据)属性claim(s)
     * 返回的可验证凭据对象不含有任何不被披露的属性claim(s)及其对应签名
     * @param vc 原始可验证凭据对象
     * @param number 待被披露的属性编号数组
     * @return 返回新的只含欲披露属性的可验证凭据对象
     */
    static public VerifiableCredential discloseClaimsByNumber(
            VerifiableCredential vc, String[] number
    ) {
        if (vc.getProof() == null) {
            throw new InvalidParameterException("请提供已被签名的可验证凭据对象");
        }
        VCProto.VerifiableCredential.Builder vcb =
                VCProto.VerifiableCredential.newBuilder();
        vcb.mergeFrom(vc.getPvc());
        if (!vcb.hasAtomicCredentialSubject()) {
            throw new InvalidParameterException("参数vc必需是基于原子性签名的可验证凭据对象");
        } else {
            Map<String, VCProto.AtomicCredentialSubject.Claim> claim = new HashMap<>();
            Map<String, String> signature = new HashMap<>();
            for (String n : number) {
                claim.put(n, vcb.getAtomicCredentialSubject()
                        .getCredentialSubjectOrThrow(n));
                signature.put(n, vcb.getProof().getAtomicSignature()
                        .getSignatureOrThrow(n));
            }
            vcb.getAtomicCredentialSubjectBuilder()
                    .clearCredentialSubject()
                    .putAllCredentialSubject(claim);
            vcb.getProofBuilder().getAtomicSignatureBuilder()
                    .clearSignature()
                    .putAllSignature(signature);

            return VerifiableCredential.parseFrom(vcb.build());
        }
    }

    /**
     * 根据claim名称选择性披露可验证凭据属性claim(s)
     * 返回的可验证凭据对象不含有任何不被披露的属性claim(s)及其对应签名
     * @param vc 原始可验证凭据对象
     * @param name 待被披露的属性名称数组
     * @return 返回新的只含欲披露属性的可验证凭据对象
     */
    static public VerifiableCredential discloseClaimsByName(
            VerifiableCredential vc, String[] name
    ) {
        if (vc.getProof() == null) {
            throw new InvalidParameterException("请提供已被签名的可验证凭据对象");
        }
        if (vc.getCredential() instanceof CredentialAtomic) {
            String[] number = new String[name.length];
            Map<String, String> name2numberMap =
                    ((CredentialAtomic) vc.getCredential()).getNameMap2Number();
            for (int i = 0; i < name.length; i++) {
                number[i] = name2numberMap.get(name[i]);
            }
            return discloseClaimsByNumber(vc, number);
        } else {
            VCProto.VerifiableCredential.Builder vcb =
                    VCProto.VerifiableCredential.newBuilder();
            vcb.mergeFrom(vc.getPvc());
            Map<String, String> claim = new HashMap<>();
            for (String n : name) {
                claim.put(n, vcb.getNormalCredentialSubject()
                        .getCredentialSubjectOrThrow(n));
            }
            vcb.getNormalCredentialSubjectBuilder()
                    .clearCredentialSubject()
                    .putAllCredentialSubject(claim);

            return VerifiableCredential.parseFrom(vcb.build());
        }
    }

    /**
     * 生成含单个可验证凭据的可验证凭据出示信息
     * @param metadata 可验证凭据出示元数据
     * @param vc 单个可验证凭据对象
     * @param sigAlg 生成可验证凭据出示信息使用的签名算法
     * @param challenge 生成可验证凭据出示信息时使用的挑战信息，验证者可根据该信息防止重放攻击等
     * @return 返回可验证凭据出示对象
     * @throws Exception
     */
    public VerifiablePresentation present(
            @Nonnull VPMetadata metadata,
            @Nonnull VerifiableCredential vc,
            @Nonnull String sigAlg,
            @Nonnull String challenge
    ) throws Exception {
        List<VerifiableCredential> vcList = new ArrayList<>();
        vcList.add(vc);
        return this.present(metadata, vcList, sigAlg, challenge);
    }

    /**
     * 生成含多个可验证凭据的可验证凭据出示信息
     * @param metadata 可验证凭据出示元数据
     * @param vcList 多个可验证凭据对象构成的List
     * @param sigAlg 生成可验证凭据出示信息使用的签名算法
     * @param challenge 生成可验证凭据出示信息时使用的挑战信息，验证者可根据该信息防止重放攻击等
     * @return 返回可验证凭据出示对象
     * @throws Exception
     */
    public VerifiablePresentation present(
            @Nonnull VPMetadata metadata,
            @Nonnull List<VerifiableCredential> vcList,
            @Nonnull String sigAlg,
            @Nonnull String challenge
    ) throws Exception {
        // 使用holder的did设置/覆盖可验证凭据出示metadata中的holder属性值
        metadata.setHolder(this.getDid());
        VerifiablePresentation vp = new VerifiablePresentation(metadata, vcList);
        vp.sign(this.getPrvKey(), this.getPublicKeyId(), sigAlg, challenge);
        return vp;
    }
}
