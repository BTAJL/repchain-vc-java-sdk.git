package com.rep.vc.exec;

import com.rep.vc.roles.CCSCreator;
import org.apache.commons.cli.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class MainClass {
    public static void main(String[] args) throws IOException {
        Options options = new Options();
        OptionGroup activityGroup = new OptionGroup();

        String helpOpName = "help";
        options.addOption("h", helpOpName, false,  "显示本帮助信息");

        String signupCCSOpName = "signupCCS";
        Option signupCCSOp = Option.builder("C")
                .argName(signupCCSOpName)
                .desc("在RepChain中创建CCS(可验证凭据属性结构)")
                .required(true)
                .longOpt(signupCCSOpName)
                .build();

        activityGroup.setRequired(false);
        activityGroup.addOption(signupCCSOp);
        options.addOptionGroup(activityGroup);

        String prvKeyPemOpName = "prvKeyPem";
        Option prvKeyPemOp = Option.builder("p")
                .argName(prvKeyPemOpName)
                .hasArg()
                .valueSeparator()
                .required(false)
                .desc("操作者的pem格式私钥文件路径")
                .longOpt(prvKeyPemOpName)
                .build();
        options.addOption(prvKeyPemOp);
        String didOpName = "did";
        Option didOp = Option.builder("d")
                .argName(didOpName)
                .hasArg()
                .valueSeparator()
                .required(false)
                .desc("操作者的did标识")
                .longOpt(didOpName)
                .build();
        options.addOption(didOp);
        String certNameOpName = "certName";
        Option certNameOp = Option.builder("c")
                .argName(certNameOpName)
                .hasArg()
                .valueSeparator()
                .required(false)
                .desc("操作者的证书名称")
                .longOpt(certNameOpName)
                .build();
        options.addOption(certNameOp);

        CommandLineParser parser = new DefaultParser();
        CommandLine line;
        try {
            line = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("Parsing failed. Reason: " + e.getMessage());
            return;
        }

        if (line.hasOption(helpOpName)) {
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("repchain-vc-java-sdk [OPTION]\n基于RepChain的可验证凭据VC Java SDK命令行工具\n", options);
            return;
        }
        if (line.hasOption(signupCCSOpName)) {
            String prvKeyPemPath = line.getOptionValue(prvKeyPemOpName);
            BufferedReader br = new BufferedReader(new FileReader(prvKeyPemPath));
            String inputLine = null;
            StringBuilder builder = new StringBuilder();
            while((inputLine = br.readLine()) != null) {
                builder.append(inputLine).append(System.lineSeparator());
            }
            String did = line.getOptionValue(didOpName);
            String certName = line.getOptionValue(certNameOpName);
            CCSCreator ccsCreator = new CCSCreator(did, certName, builder.toString());
            System.out.println(ccsCreator);
            return;
        }
        System.out.println("Hello, World!");
    }
}
