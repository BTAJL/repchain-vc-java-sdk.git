package com.rep.vc.utils;

import com.rep.vc.models.vc.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class VerifiableCredentials {
    public VCMetadata metadata;
    public Map<String, VCClaim> credentialSubjectAtomic;
    public Map<String, String> credentialSubjectNormal;
    public CredentialAtomic credentialAtomic;
    public CredentialNormal credentialNormal;

    public VerifiableCredentials() {
        this.metadata = new VCMetadata();
        this.metadata.setId(UUID.randomUUID().toString());
        this.metadata.addType("UniversityDegreeCredential");
        this.metadata.setClaimScheme("CCS-001");
        this.metadata.setIssuer("did:rep:identity-net:123456");
        this.metadata.setIssued("2022-05-26T10:08:09Z");
        this.metadata.setValidFrom("2022-05-26T10:08:59Z");
        this.metadata.setValidUntil("2099-05-26T10:08:59Z");

        VCClaim[]  claims = {
                new VCClaim("serialNumber", "2022052612342176"),
                new VCClaim("id", "did:rep:identity-net:987654"),
                new VCClaim("name", "Tom"),
                new VCClaim("degree", "Master"),
                new VCClaim("date", "2021-06-30"),
                new VCClaim("university", "RepChain Training University")
        };

        credentialSubjectAtomic = new HashMap<>();
        for (int i = 0; i < claims.length; i++) {
            credentialSubjectAtomic.put("" + i, claims[i]);
        }
        credentialAtomic =
                new CredentialAtomic(metadata, credentialSubjectAtomic);

        credentialSubjectNormal = new HashMap<>();
        for (int i = 0; i < claims.length; i++) {
            credentialSubjectNormal.put(claims[i].getName(), claims[i].getValue());
        }
        credentialNormal =
                new CredentialNormal(metadata, credentialSubjectNormal);
    }
}
