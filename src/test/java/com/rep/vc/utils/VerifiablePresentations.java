package com.rep.vc.utils;

import com.rep.vc.models.vc.VerifiableCredential;
import com.rep.vc.models.vp.VPMetadata;
import com.rep.vc.roles.VCIssuer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VerifiablePresentations {
    public VPMetadata metadata;
    public List<VerifiableCredential> verifiableCredentialListMultiVC =
            new ArrayList<>();
    public List<VerifiableCredential> verifiableCredentialListOnlyOneVC =
            new ArrayList<>();
    public VerifiableCredential verifiableCredential1;
    public VerifiableCredential verifiableCredential2;
    public VCIssuer issuer;

    public VerifiablePresentations() {
        this.metadata = new VPMetadata();
        this.metadata.addType("UniversityDegreePresentation");
        this.metadata.setId(UUID.randomUUID().toString());
        this.metadata.setHolder(CryptoMaterials.did2);

        try {
            issuer = new VCIssuer(
                    CryptoMaterials.did1,
                    CryptoMaterials.certName1,
                    CryptoMaterials.workingPrvKeyPemStr1
            );
            verifiableCredential1 = issuer.issueVerifiableCredential(
                    new VerifiableCredentials().credentialAtomic,
                    SignatureTools.ECSecp256k1Sig
            );
            verifiableCredential2 = issuer.issueVerifiableCredential(
                    new VerifiableCredentials().credentialAtomic,
                    SignatureTools.ECSecp256k1Sig
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

        verifiableCredentialListMultiVC.add(verifiableCredential1);
        verifiableCredentialListMultiVC.add(verifiableCredential2);

        verifiableCredentialListOnlyOneVC.add(verifiableCredential1);
    }
}
