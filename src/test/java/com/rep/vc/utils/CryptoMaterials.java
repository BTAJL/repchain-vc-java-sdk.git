package com.rep.vc.utils;

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;

import java.io.IOException;
import java.io.StringWriter;
import java.security.*;

public class CryptoMaterials {
    public static String accountId1 = "121000005l35120456";
    public static String certName1 = "node1";
    public static String did1 = "did:rep:identity-net:121000005l35120456";
    public static String accountId2 = "12110107bi45jh675g";
    public static String certName2 = "node2";
    public static String did2 = "did:rep:identity-net:12110107bi45jh675g";
    public static PrivateKey prvKey;
    public static PublicKey pubKey;
    public static String prvKeyPemStr;

    static {
        Security.addProvider(new BouncyCastleProvider());
        ECParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec("secp256k1");
        try {
            KeyPairGenerator generator = KeyPairGenerator
                    .getInstance("ECDSA", "BC");
            generator.initialize(ecSpec, new SecureRandom());
            KeyPair keypair = generator.generateKeyPair();
            prvKey = keypair.getPrivate();
            pubKey = keypair.getPublic();
            StringWriter sw = new StringWriter();
            PemWriter pemWriter = new PemWriter(sw);
            pemWriter.writeObject(new PemObject("PRIVATE KEY", prvKey.getEncoded()));
            pemWriter.flush();
            prvKeyPemStr = sw.toString();
        } catch (IOException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }
    public static String workingPrvKeyPemStr1 = "-----BEGIN PRIVATE KEY-----\n" +
    "MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQg/i8znMeuOO1eANT/\n" +
    "KBdGy2d5L/7h/zcg1KbgCaBS6eugCgYIKoZIzj0DAQehRANCAAS2dFJZHZe07B9c\n" +
    "ZAugHd1dotPTAf8Lb05dW/644EXqTaXst4MzVt43grLpuhEkvdtFB6n75yOrc0zQ\n" +
    "+Q5fUFuQ\n" +
    "-----END PRIVATE KEY-----"
    ;
    public static PrivateKey workingPrvKey1;
    public static String workingPrvKeyPemStr2 = "-----BEGIN PRIVATE KEY-----\n" +
    "MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgO4FKBSrIPL3cCzKx\n" +
    "M6ACecm6JI1bm0p47eIvDlaCsE6gCgYIKoZIzj0DAQehRANCAAT9CcKGs5t447n4\n" +
    "j7qqsjd5zsc9EUcXCPRlvVYa0HHllEvZhLiLn8PFXIBGyc/4DVlgbaSoP+R+DCBx\n" +
    "c4ctlA/x\n" +
    "-----END PRIVATE KEY-----"
    ;
    public static PrivateKey workingPrvKey2;

    static {
        try {
            workingPrvKey1 = SignatureTools.getPrvKey(workingPrvKeyPemStr1);
            workingPrvKey2 = SignatureTools.getPrvKey(workingPrvKeyPemStr2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
