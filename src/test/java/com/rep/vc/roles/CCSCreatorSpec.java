package com.rep.vc.roles;

import com.rep.vc.models.ccs.CredentialClaimStruct;
import com.rep.vc.models.ccs.CredentialClaimStructAttr;
import com.rep.vc.utils.ConfigExtension;
import com.rep.vc.utils.CryptoMaterials;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.security.*;

import static com.rep.vc.utils.Misc.getCurrentISODateTime;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("测试凭据属性结构创建者CCSCreator相关功能")
@ExtendWith(ConfigExtension.class)
public class CCSCreatorSpec {
    String did;
    String certName;
    PrivateKey prvKey;
    String prvKeyPemStr;
    String workingPrvKeyPemStr;
    CredentialClaimStructAttr[] attrs;
    CredentialClaimStruct ccs;

    @BeforeAll
    void init() {
        did = CryptoMaterials.did1;
        certName = CryptoMaterials.certName1;
        prvKey = CryptoMaterials.prvKey;
        prvKeyPemStr = CryptoMaterials.prvKeyPemStr;
        workingPrvKeyPemStr = CryptoMaterials.workingPrvKeyPemStr1;

        attrs = new CredentialClaimStructAttr[6];
        attrs[0] = new CredentialClaimStructAttr("serialNumber", "String", true, "学位证书编号");
        attrs[1] = new CredentialClaimStructAttr("id", "String", true, "学位证书获得者did标识");
        attrs[2] = new CredentialClaimStructAttr("name", "String", true, "学位获得者姓名");
        attrs[3] = new CredentialClaimStructAttr("degree", "String", true, "学位名称");
        attrs[4] = new CredentialClaimStructAttr("date", "String", true, "学位授予日期");
        attrs[5] = new CredentialClaimStructAttr("university", "String", true, "学位授予学校(单位)");
        ccs = new CredentialClaimStruct(
                "CCS-001",
                "UniversityDegreeCredential",
                "1.0",
                "高等学校学位证书",
                attrs
        );
    }

    @Test
    @DisplayName("使用私钥PEM字符串和私钥对象构造CCSCreator应等价")
    public void testConstructor() throws IOException {
        CCSCreator creatorFromPemStr = new CCSCreator(did, certName, prvKeyPemStr);
        CCSCreator creatorFromFromPrvKey = new CCSCreator(did, certName, prvKey);
        assertNotNull(creatorFromPemStr);
        assertNotNull(creatorFromFromPrvKey);
        assertArrayEquals(creatorFromFromPrvKey.getPrvKey().getEncoded(),
                creatorFromPemStr.getPrvKey().getEncoded());

    }

    @Test
    @DisplayName("CCSCreator向RepChain网络注册ccs应能成功")
    public void testSignupCCS2RepChain() throws Exception {
        CCSCreator creator = new CCSCreator(did, certName, workingPrvKeyPemStr);
        String res = creator.signupCCS2RepChain(ccs);
        assertNotNull(res);
    }

    @Test
    @DisplayName("CCSCreator向RepChain网络更新ccs状态应能成功")
    public void testUpdateCCSStatus() throws Exception {
        CCSCreator creator = new CCSCreator(did, certName, workingPrvKeyPemStr);
        String res = creator.updateCCSStatus(ccs.getId(), false);
        assertNotNull(res);
    }
}
