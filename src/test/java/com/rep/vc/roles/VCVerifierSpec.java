package com.rep.vc.roles;

import com.rep.vc.models.vc.VerifiableCredential;
import com.rep.vc.models.vp.VPMetadata;
import com.rep.vc.models.vp.VerifiablePresentation;
import com.rep.vc.utils.ConfigExtension;
import com.rep.vc.utils.CryptoMaterials;
import com.rep.vc.utils.SignatureTools;
import com.rep.vc.utils.VerifiablePresentations;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;

@DisplayName("测试凭据验证者VCIssuer相关功能")
@ExtendWith(ConfigExtension.class)
public class VCVerifierSpec {
    VCIssuer issuer;
    VCHolder holder = new VCHolder(
            CryptoMaterials.did1,
            CryptoMaterials.certName1,
            CryptoMaterials.workingPrvKey1
    );
    VerifiableCredential vc;
    VerifiablePresentation vp;
    String challenge = "challenge-random-098cdsh";
    VCVerifier verifier = new VCVerifier(
            "did:rep:identity-net:123456",
            "certTest",
            CryptoMaterials.prvKey
    );
    String initialStatus;
    String id;

    @BeforeEach
    public void init4Each() throws Exception {
        VerifiablePresentations vps = new VerifiablePresentations();
        issuer = vps.issuer;
        vc = vps.verifiableCredential1;
        VPMetadata metadata = vps.metadata;
        List<VerifiableCredential> vcList = new ArrayList<>();
        vcList.add(vc);
        vp = holder.present(
                metadata,
                vcList,
                SignatureTools.ECSecp256k1Sig,
                challenge
        );
        id = vc.getCredential().getMeta().getId();
        initialStatus = "VALID" ;
        issuer.signupVCStatus(id, initialStatus);
        Thread.sleep(5000);
    }

    @Test
    @Timeout(19)
    @DisplayName("对可验证凭据出示信息VerifiablePresentation进行验证应能成功")
    public void testVerifyVerifiablePresentation() throws Exception {
        Thread.sleep(3000);
        String[] status = new String[]{"VALID"};
        boolean res = VCVerifier.verify(vp, challenge, status);
        Assertions.assertTrue(res);

        issuer.updateVCStatus(id, "INVALID");
        Thread.sleep(5000);
        res = VCVerifier.verify(vp, challenge, status);
        Assertions.assertFalse(res);

        issuer.updateVCStatus(id, "VALID");
        Thread.sleep(5000);
        res = VCVerifier.verify(vp, challenge, status);
        Assertions.assertTrue(res);
        issuer.revokeVCClaims(id, new String[]{"1", "2"});
        Thread.sleep(5000);
        res = VCVerifier.verify(vp, challenge, status);
        Assertions.assertFalse(res);
    }
}
