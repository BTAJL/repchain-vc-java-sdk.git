package com.rep.vc.roles;

import com.rep.vc.models.vc.VCClaim;
import com.rep.vc.models.vc.VerifiableCredential;
import com.rep.vc.models.vp.VPMetadata;
import com.rep.vc.models.vp.VerifiablePresentation;
import com.rep.vc.utils.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@DisplayName("测试凭据持有者VCHolder相关功能")
@ExtendWith(ConfigExtension.class)
public class VCHolderSpec {
    String did = CryptoMaterials.did1;
    String certName = CryptoMaterials.certName1;
    PrivateKey prvKey = CryptoMaterials.workingPrvKey1;
    VCHolder holder = new VCHolder(did, certName, prvKey);
    VCIssuer issuer = new VCIssuer(did, certName, prvKey);
    VerifiableCredentials vcs;
    VerifiablePresentations vps;

    VerifiableCredential vcAtomic;
//    VerifiableCredential vcNormal;

    @BeforeEach
    public void init4Each() throws Exception {
        vcs = new VerifiableCredentials();
        vcAtomic = issuer.issueVerifiableCredential(
                vcs.credentialAtomic,
                SignatureTools.ECSecp256k1Sig
        );
//        vcNormal = new VerifiableCredential(vcs.credentialNormal);
        vps = new VerifiablePresentations();
    }

    @Test
    @DisplayName("根据属性编号选择性披露可验证凭据应能成功")
    public void testDiscloseClaimsByNumber() {
        String[] number = new String[]{"0", "1", "2"};
        VerifiableCredential disclosedVC =
                VCHolder.discloseClaimsByNumber(vcAtomic, number);
        Map<String, VCClaim> disclosedClaims = (Map<String, VCClaim>) disclosedVC.getCredential().getCredentialSubject();
        Map<String, String>  disclosedSigantures = (Map<String, String>) disclosedVC.getProof().getSignature();
        Assertions.assertEquals(
                disclosedClaims.size(), number.length
        );
        Assertions.assertEquals(
                disclosedSigantures.size(), number.length
        );
        Assertions.assertTrue(
                vcAtomic.getCredential().getClaimCount() >
                        disclosedVC.getCredential().getClaimCount()
        );
    }

    @Test
    @DisplayName("根据属性名称选择性披露可验证凭据应能成功")
    public void testDiscloseClaimsByrName() {
        String[] name = new String[]{"name", "id", "degree"};
        VerifiableCredential disclosedVC =
                VCHolder.discloseClaimsByName(vcAtomic, name);
        Map<String, VCClaim> disclosedClaims = (Map<String, VCClaim>) disclosedVC.getCredential().getCredentialSubject();
        Map<String, String>  disclosedSigantures = (Map<String, String>) disclosedVC.getProof().getSignature();
        Assertions.assertEquals(
                disclosedClaims.size(), name.length
        );
        Assertions.assertEquals(
                disclosedSigantures.size(), name.length
        );
        Assertions.assertTrue(
                vcAtomic.getCredential().getClaimCount() >
                        disclosedVC.getCredential().getClaimCount()
        );
    }

    @Test
    @DisplayName("不能对未被签名的可验证凭据进行选择性披露")
    public void testDiscloseClaimsWithNotSignedVC() {
        String[] name = new String[]{"name", "id", "degree"};
        VerifiableCredential vc = new VerifiableCredential(
                vcs.credentialAtomic
        );
        Assertions.assertThrows(
                InvalidParameterException.class,
                () -> VCHolder.discloseClaimsByName(vc, name)
        );
    }

    @Test
    @DisplayName("对经选择性披露后的可验证凭据进行验签应能成功")
    public void testVerifyDisclosedClaims() throws IOException {
        String[] name = new String[]{"name", "id", "degree"};
        VerifiableCredential vc = VCHolder.discloseClaimsByName(vcAtomic, name);
        Assertions.assertTrue(vc.verifySignature());
    }

    @Test
    @DisplayName("生成可验证凭据出示信息Verifiable Presentation应能成功")
    public void testPresent() throws Exception {
        String[] name = new String[]{"name", "id", "degree"};
        VerifiableCredential vc = VCHolder.discloseClaimsByName(
                vps.verifiableCredential1,
                name
        );
        VPMetadata metadata = vps.metadata;
        List<VerifiableCredential> vcList = new ArrayList<>();
        vcList.add(vc);
        VerifiablePresentation vp = holder.present(metadata, vcList, SignatureTools.ECSecp256k1Sig, "challenge-bcdhjs");
        Assertions.assertNotNull(vp);
        Assertions.assertNotNull(vp.getMetadata());
        Assertions.assertNotNull(vp.getVerifiableCredential());
        Assertions.assertNotNull(vp.getProof());
        Assertions.assertNotNull(vp.getPvp());
        Assertions.assertTrue(vp.verifySignature());
    }
}
