package com.rep.vc.roles;

import com.rep.vc.models.vc.*;
import com.rep.vc.utils.ConfigExtension;
import com.rep.vc.utils.CryptoMaterials;
import com.rep.vc.utils.SignatureTools;
import com.rep.vc.utils.VerifiableCredentials;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("测试凭据签发者VCIssuer相关功能")
@ExtendWith(ConfigExtension.class)
public class VCIssuerSpec {
    String prvKeyPemStr;
    String certName;
    String did;
    VCIssuer issuer;
    VerifiableCredentials vcs;

    @BeforeAll
    public void initAll() throws IOException {
         prvKeyPemStr = CryptoMaterials.workingPrvKeyPemStr1;
         certName = CryptoMaterials.certName1;
         did = CryptoMaterials.did1;
         issuer = new VCIssuer(did, certName, prvKeyPemStr);
    }

    @BeforeEach
    public void init4Each() throws Exception {
        vcs = new VerifiableCredentials();
    }

    @Test
    @DisplayName("签发可验证凭据应能成功")
    public void testIssueVerifiableCredential() throws Exception {
        Credential credential = vcs.credentialAtomic;
        VerifiableCredential vc = issuer.issueVerifiableCredential(
                credential,
                SignatureTools.ECSecp256k1Sig
        );
        Assertions.assertNotNull(vc);
        Assertions.assertNotNull(vc.getProof());
        Assertions.assertNotNull(vc.getPvc());
        Assertions.assertNotNull(vc.exportAsJsonStr());
        Assertions.assertNotNull(vc.exportAsBytes());
    }

    @Test
    @DisplayName("向RepChain注册可验证凭据状态信息应能成功")
    public void testSignupVCStatus() throws Exception {
        String res = issuer.signupVCStatus(
                vcs.metadata.getId(), "NOT_ACTIVE"
        );
        Assertions.assertTrue(res.contains("txid"));
    }

    @Test
    @DisplayName("向RepChain更新可验证凭据状态信息应能成功")
    public void testUpdateVCStatus() throws Exception {
        String res = issuer.updateVCStatus(
                vcs.metadata.getId(), "VALID"
        );
        Assertions.assertTrue(res.contains("txid"));
    }

    @Test
    @DisplayName("向RepChain撤销可验证凭据属性Claim应能成功")
    public void testRevokeVCClaims() throws Exception {
        String[] revokedClaimIndex = new String[]{"1", "3", "7"};
        String res = issuer.revokeVCClaims(
                vcs.metadata.getId(), revokedClaimIndex
        );
        Assertions.assertTrue(res.contains("txid"));
    }
}
