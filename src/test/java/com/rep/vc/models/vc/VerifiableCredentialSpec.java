package com.rep.vc.models.vc;

import com.rep.vc.utils.ConfigExtension;
import com.rep.vc.utils.CryptoMaterials;
import com.rep.vc.utils.VerifiableCredentials;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import java.security.*;
import java.util.Map;

import static com.rep.vc.utils.SignatureTools.ECSecp256k1Sig;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("测试VerifiableCredential数据结构相关功能")
@ExtendWith(ConfigExtension.class)
public class VerifiableCredentialSpec {

    CredentialAtomic credentialAtomic;
    CredentialNormal credentialNormal;
    VerifiableCredential vcAtomic;
    VerifiableCredential vcNormal;
    PrivateKey prvKey = CryptoMaterials.prvKey;
    PublicKey pubKey = CryptoMaterials.pubKey;

    @BeforeEach
    public void init4Each() {
        VerifiableCredentials vcs = new VerifiableCredentials();
        credentialAtomic = vcs.credentialAtomic;
        credentialNormal = vcs.credentialNormal;
        vcAtomic = new VerifiableCredential(credentialAtomic);
        vcNormal = new VerifiableCredential(credentialNormal);
    }

    @Test
    @DisplayName("创建基于原子性签名的VerifiableCredential对象应能成功")
    public void testCreateAtomicVerifiableCredential() {
        Assertions.assertEquals(vcAtomic.getCredential(), credentialAtomic);
        Assertions.assertNotNull(vcAtomic.getPvc());
        Assertions.assertTrue(vcAtomic.getPvc().hasAtomicCredentialSubject());
        // proof should be null after initialization
        Assertions.assertNull(vcAtomic.getProof());
        Assertions.assertFalse(vcAtomic.getPvc().hasProof());
    }

    @Test
    @DisplayName("创建非基于原子性签名的VerifiableCredential对象应能成功")
    public void testCreateNormalVerifiableCredential() {
        Assertions.assertEquals(vcNormal.getCredential(), credentialNormal);
        Assertions.assertNotNull(vcNormal.getPvc());
        Assertions.assertTrue(vcNormal.getPvc().hasNormalCredentialSubject());
        // proof should be null after initialization
        Assertions.assertNull(vcNormal.getProof());
        Assertions.assertFalse(vcNormal.getPvc().hasProof());
    }

    @Test
    @DisplayName("为基于原子性签名的VerifiableCredential生成签名信息应能成功")
    @SuppressWarnings("unchecked")
    public void testSignAtomicVerifiableCredential() throws Exception {
        vcAtomic.sign(prvKey, "did:rep:identity-net:123456#key1", ECSecp256k1Sig);
        Assertions.assertNotNull(vcAtomic.getProof());
        Assertions.assertEquals(
                ((Map<String, String>) vcAtomic.getProof().getSignature()).size(),
                vcAtomic.getCredential().getClaimCount()
        );
        Assertions.assertTrue(vcAtomic.getPvc().hasProof());
        Assertions.assertTrue(vcAtomic.getPvc().getProof().hasAtomicSignature());
        Assertions.assertEquals(
                vcAtomic.getPvc().getProof().getAtomicSignature().getSignatureCount(),
                vcAtomic.getCredential().getClaimCount()
        );
    }

    @Test
    @DisplayName("不能对已被签名的VerifiableCredential再次签名")
    public void testSignAtomicVerifiableCredentialAgain() throws Exception {
        vcAtomic.sign(prvKey, "did:rep:identity-net:123456#key1", ECSecp256k1Sig);
        Assertions.assertThrows(
                Exception.class,
                () -> vcAtomic.sign(prvKey, "did:rep:identity-net:123456#key1", ECSecp256k1Sig)
        );
    }

    @Test
    @DisplayName("对基于原子性签名的VerifiableCredential进行签名信息验证，手动提供对应公钥时应能成功")
    @SuppressWarnings("unchecked")
    public void testVerifyAtomicVerifiableCredentialWithPubkey() throws Exception {
        vcAtomic.sign(prvKey, "did:rep:identity-net:123456#key1", ECSecp256k1Sig);
        boolean res = vcAtomic.verifySignature(pubKey);
        Assertions.assertTrue(res);
    }

    @Test
    @DisplayName("对基于原子性签名的VerifiableCredential进行签名信息验证，默认从RepChain获取公钥时应能成功")
    @SuppressWarnings("unchecked")
    public void testVerifyAtomicVerifiableCredentialWithoutPubkey() throws Exception {
        vcAtomic.sign(
                CryptoMaterials.workingPrvKey1,
                CryptoMaterials.did1 + "#" + CryptoMaterials.certName1,
                ECSecp256k1Sig
        );
        boolean res = vcAtomic.verifySignature();
        Assertions.assertTrue(res);
    }

    @Test
    @DisplayName("为非基于原子性签名的VerifiableCredential生成签名信息应能成功")
    public void testSignNormalVerifiableCredential() throws Exception {
//        vcNormal.sign(prvKey, "did:rep:identity-net:123456#key1", ECSecp256k1Sig, ONE_HASH_SIG_SCHEME);
//        Assertions.assertNotNull(vcNormal.getProof());
//        Assertions.assertTrue((String)vcNormal.getProof().getSignature() != "" );
//        Assertions.assertEquals(
//                ((VCProto.VerifiableCredential) vcNormal.getPvc().getVc())
//                        .getProof().getSignature(),
//                (String)vcNormal.getProof().getSignature()
//        );
    }

    @Test
    @DisplayName("将VerifiableCredential导出为二进制数据应能成功")
    public void testExportVerifiableCredentialAsBytes() throws Exception {
        vcAtomic.sign(prvKey, "did:rep:identity-net:123456#key1", ECSecp256k1Sig);
        byte[] vcBytes = vcAtomic.exportAsBytes();
        Assertions.assertNotNull(vcBytes);
    }

    @Test
    @DisplayName("将二进制数据反解析为VerifiableCredential对象应能成功")
    public void testParseFromBytes() throws Exception {
        vcAtomic.sign(prvKey, "did:rep:identity-net:123456#key1", ECSecp256k1Sig);
        byte[] vcBytes = vcAtomic.exportAsBytes();
        VerifiableCredential vcParsed = VerifiableCredential.parseFrom(vcBytes);
        Assertions.assertNotNull(vcParsed);
        Assertions.assertNotNull(vcParsed.getCredential());
        Assertions.assertNotNull(vcParsed.getProof());
        Assertions.assertNotNull(vcParsed.getPvc());
        Assertions.assertTrue(vcParsed.verifySignature(pubKey));
    }

    @Test
    @DisplayName("将VerifiableCredential导出为Json字符串应能成功")
    public void testExportVerifiableCredentialAsJsonStr() throws Exception {
        vcAtomic.sign(prvKey, "did:rep:identity-net:123456#key1", ECSecp256k1Sig);
        String vcJsonStr = vcAtomic.exportAsJsonStr();
        Assertions.assertTrue(vcJsonStr != "");
    }

    @Test
    @DisplayName("将Json字符串反解析为VerifiableCredential对象应能成功")
    public void testParseFromJsonStr() throws Exception {
        vcAtomic.sign(prvKey, "did:rep:identity-net:123456#key1", ECSecp256k1Sig);
        String vcJsonStr = vcAtomic.exportAsJsonStr();
        VerifiableCredential vcParsed = VerifiableCredential.parseFrom(vcJsonStr);
        Assertions.assertNotNull(vcParsed);
        Assertions.assertNotNull(vcParsed.getCredential());
        Assertions.assertNotNull(vcParsed.getProof());
        Assertions.assertNotNull(vcParsed.getPvc());
        Assertions.assertTrue(vcParsed.verifySignature(pubKey));
    }
}
