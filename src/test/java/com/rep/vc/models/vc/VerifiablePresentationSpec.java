package com.rep.vc.models.vc;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rep.vc.models.vp.VPMetadata;
import com.rep.vc.utils.ConfigExtension;
import com.rep.vc.utils.CryptoMaterials;
import com.rep.vc.utils.SignatureTools;
import com.rep.vc.utils.VerifiablePresentations;
import com.rep.vc.models.vp.VerifiablePresentation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.security.PrivateKey;
import java.util.Arrays;
import java.util.List;

@DisplayName("测试VerifiablePresentation数据结构相关功能")
@ExtendWith(ConfigExtension.class)
public class VerifiablePresentationSpec {
    VerifiablePresentation vpWithMultiVC;
    VerifiablePresentation vpWithOnlyOneVC;

    private void signVP(VerifiablePresentation vp) throws Exception {
        signVP(vp, CryptoMaterials.workingPrvKey2);
    }
    private void signVP(VerifiablePresentation vp, PrivateKey prvKey) throws Exception {
        vp.sign(
                prvKey,
                CryptoMaterials.did2 + "#" + CryptoMaterials.certName2,
                SignatureTools.ECSecp256k1Sig,
                "challenge-0"
        );
    }

    @BeforeEach
    public void init4Each() {
        VerifiablePresentations vps = new VerifiablePresentations();
        VPMetadata metadata = vps.metadata;
        List<VerifiableCredential> verifiableCredentialListMultiVC = vps.verifiableCredentialListMultiVC;
        List<VerifiableCredential> verifiableCredentialListOnlyOneVC = vps.verifiableCredentialListOnlyOneVC;
        vpWithMultiVC = new VerifiablePresentation(
                metadata, verifiableCredentialListMultiVC
        );
        vpWithOnlyOneVC = new VerifiablePresentation(
                metadata, verifiableCredentialListOnlyOneVC
        );
    }

    @Test
    @DisplayName("创建可验证凭据出示VerifiablePresentation对象应能成功")
    public void testConstructVerifiablePresentation() {
        Assertions.assertNotNull(vpWithMultiVC);
        Assertions.assertNotNull(vpWithMultiVC.getMetadata());
        Assertions.assertNotNull(vpWithMultiVC.getVerifiableCredential());
        Assertions.assertNotNull(vpWithMultiVC.getPvp());
        Assertions.assertNull(vpWithMultiVC.getProof());
    }

    @Test
    @DisplayName("为可验证凭据出示VerifiablePresentation对象签名应能成功")
    public void testSignVerifiablePresentation() throws Exception {
        signVP(vpWithMultiVC);
        Assertions.assertNotNull(vpWithMultiVC.getProof());
        Assertions.assertNotNull(vpWithMultiVC.getPvp().getProof().getSignature());
    }


    @Test
    @DisplayName("不能对已被签名的可验证凭据出示VerifiablePresentation再次签名")
    public void testSignVerifiablePresentationAgain() throws Exception {
        signVP(vpWithMultiVC);
        Assertions.assertThrows(Exception.class, () -> signVP(vpWithMultiVC));
    }

    @Test
    @DisplayName("对已被签名的可验证凭据出示VerifiablePresentation进行验证签名，手动提供对应公钥时应能成功")
    public void testVerifyVerifiablePresentationWithPubKey() throws Exception {
        signVP(vpWithMultiVC, CryptoMaterials.prvKey);
        boolean res = vpWithMultiVC.verifySignature(CryptoMaterials.pubKey);
        Assertions.assertTrue(res);
    }

    @Test
    @DisplayName("对已被签名的可验证凭据出示VerifiablePresentation进行验证签名，默认从RepChain获取公钥时应能成功")
    public void testVerifyVerifiablePresentationWithoutPubKey() throws Exception {
        signVP(vpWithMultiVC);
        boolean res = vpWithMultiVC.verifySignature();
        Assertions.assertTrue(res);
    }
    @Test
    @DisplayName("将可验证凭据出示VerifiablePresentation对象导出为二进制数据应能成功")
    public void testExportVerifiablePresentationAsBytes() throws Exception {
        signVP(vpWithMultiVC);
        byte[] bytes = vpWithMultiVC.exportAsBytes();
        Assertions.assertNotNull(bytes);
    }

    @Test
    @DisplayName("将二进制数据反解析为可验证凭据出示VerifiablePresentation对象应能成功")
    public void testParseFromBytes() throws Exception {
        signVP(vpWithMultiVC);
        byte[] bytes = vpWithMultiVC.exportAsBytes();
        VerifiablePresentation vpParsed = VerifiablePresentation.parseFrom(bytes);
        Assertions.assertNotNull(vpParsed);
        Assertions.assertNotNull(vpParsed.getPvp());
        Assertions.assertNotNull(vpParsed.getProof());
        Assertions.assertNotNull(vpParsed.getVerifiableCredential());
        Assertions.assertNotNull(vpParsed.getMetadata());
        Assertions.assertNotEquals(vpWithMultiVC, vpParsed);
    }

    @Test
    @DisplayName("将可验证凭据出示VerifiablePresentation对象导出为Json字符串应能成功")
    public void testExportVerifiablePresentationAsJsonStr() throws Exception {
        signVP(vpWithMultiVC);
        String jsonStrWithMultiVC = vpWithMultiVC.exportAsJsonStr();
        signVP(vpWithOnlyOneVC);
        String jsonStrWithOnlyOneVC = vpWithOnlyOneVC.exportAsJsonStr();
        Assertions.assertNotNull(jsonStrWithMultiVC);
        Assertions.assertNotNull(jsonStrWithOnlyOneVC);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode4VPWithMultiVC = mapper.readTree(jsonStrWithMultiVC);
        JsonNode jsonNode4VPWithOnlyOneVC = mapper.readTree(jsonStrWithOnlyOneVC);
        Assertions.assertTrue(
                jsonNode4VPWithMultiVC.get("verifiableCredential").isArray()
        );
        Assertions.assertTrue(
                jsonNode4VPWithOnlyOneVC.get("verifiableCredential").isObject()
        );
    }

    @Test
    @DisplayName("将Json字符串反序列化可验证凭据出示VerifiablePresentation对象应能成功")
    public void testParseFromJsonStr() throws Exception {
        signVP(vpWithMultiVC);
        String jsonStrWithMultiVC = vpWithMultiVC.exportAsJsonStr();
        signVP(vpWithOnlyOneVC);
        String jsonStrWithOnlyOneVC = vpWithOnlyOneVC.exportAsJsonStr();
        ObjectMapper mapper = new ObjectMapper();
        VerifiablePresentation vpWithMultiVCParsed =
                mapper.readValue(jsonStrWithMultiVC, VerifiablePresentation.class);
        VerifiablePresentation vpWithOnlyOneVCParsed =
                mapper.readValue(jsonStrWithOnlyOneVC, VerifiablePresentation.class);
        Assertions.assertNotNull(vpWithMultiVCParsed);
        Assertions.assertNotNull(vpWithOnlyOneVCParsed);
        Assertions.assertTrue(
                Arrays.equals(
                        vpWithOnlyOneVCParsed.exportAsBytes(),
                        vpWithOnlyOneVC.exportAsBytes()
                )
        );
        Assertions.assertEquals(
                vpWithMultiVCParsed.exportAsJsonStr(),
                vpWithMultiVC.exportAsJsonStr()
        );
    }
}
