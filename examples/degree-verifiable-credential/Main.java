package com.rep.vc.examples.degree;

import com.rep.vc.utils.Config;
import com.rep.vc.utils.SignatureTools;
import com.rep.vc.models.ccs.CredentialClaimStruct;
import com.rep.vc.models.ccs.CredentialClaimStructAttr;
import com.rep.vc.roles.CCSCreator;
import com.rep.vc.roles.VCIssuer;
import com.rep.vc.roles.VCHolder;
import com.rep.vc.roles.VCVerifier;
import com.rep.vc.models.vc.VCMetadata;
import com.rep.vc.models.vc.VCClaim;
import com.rep.vc.models.vc.Credential;
import com.rep.vc.models.vc.CredentialAtomic;
import com.rep.vc.models.vc.VerifiableCredential;
import com.rep.vc.models.vp.VPMetadata;
import com.rep.vc.models.vp.VerifiablePresentation;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        // 配置欲连接的RepChain节点地址
        Config.getInstance().setPeerEndpoint("htto://localhost:8081");

        // ccs创建者did，账户成功注册后会生成，其格式为did:rep:<网络名>:<账户标识>
        String ccsCreatorDid = "did:rep:identity-net:123456";
        // ccs创建者本次操作使用的证书名称
        String ccsCreatorCertName = "creator-1";
        // ccs创建者本次操作使用的私钥(pem格式字符串，也可为PrivateKey对象)
        String ccsCreatorPrvKey = "-----BEGIN PRIVATE KEY-----\nMIGTAgEAMBMGByq........\n-----END PRIVATE KEY-----";
        CCSCreator creator = new CCSCreator(ccsCreatorDid, ccsCreatorCertName, ccsCreatorPrvKey);
        // 构建ccs对象
        CredentialClaimStructAttr[] attrs = new CredentialClaimStructAttr[6];
        attrs[0] = new CredentialClaimStructAttr("serialNumber", "String", true, "学位证书编号");
        attrs[1] = new CredentialClaimStructAttr("id", "String", true, "学位证书获得者did标识");
        attrs[2] = new CredentialClaimStructAttr("name", "String", true, "学位获得者姓名");
        attrs[3] = new CredentialClaimStructAttr("degree", "String", true, "学位名称");
        attrs[4] = new CredentialClaimStructAttr("date", "String", true, "学位授予日期");
        attrs[5] = new CredentialClaimStructAttr("university", "String", true, "学位授予学校(单位)");
        CredentialClaimStruct ccs = new CredentialClaimStruct(
                "CCS-001",
                "UniversityDegreeCredential",
                "1.0",
                "高等学校学位证书",
                attrs
        );
        // 注册ccs到RepChain区块链
        try {
            creator.signupCCS2RepChain(ccs);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 凭据颁发者did
        String issuerDid = "did:rep:identity-net:123";
        // 凭据颁发者本次操作所用证书名称
        String issuerCertName = "issuer-1";
        // 凭据颁发者本次操作所用私钥(pem格式字符串，也可为PrivateKey对象)
        String issuerPrvKey = "-----BEGIN PRIVATE KEY-----\nMIGTAgEAMBMGByq........\n-----END PRIVATE KEY-----";
        VCIssuer issuer = new VCIssuer(issuerDid, issuerCertName, issuerPrvKey);
        // 凭据元数据
        VCMetadata metadata = new VCMetadata();
        metadata.setId("0x123456");
        metadata.addType("UniversityDegreeCredential");
        metadata.setClaimScheme("CCS-001");
        metadata.setValidFrom("2021-03-26T10:08:59Z");
        metadata.setValidUntil("2022-03-26T10:08:59Z");
        // 凭据属性
        Map<String, VCClaim> credentialSubjectAtomic = new HashMap<>();
        credentialSubjectAtomic.put("1", new VCClaim("serialNumber", "2021063012342176"));
        credentialSubjectAtomic.put("2", new VCClaim("id", "did:rep:identity-net:987654"));
        credentialSubjectAtomic.put("3", new VCClaim("name", "Tom"));
        credentialSubjectAtomic.put("4", new VCClaim("degree", "Master"));
        credentialSubjectAtomic.put("5", new VCClaim("date", "2021-06-30"));
        credentialSubjectAtomic.put("6", new VCClaim("university", "RepChain Training University"));
        // 创建凭据对象
        Credential credential = new CredentialAtomic(metadata, credentialSubjectAtomic);
        // 凭据颁发者签发可验证凭据
        VerifiableCredential vc = issuer.issueVerifiableCredential(
                credential,
                SignatureTools.ECSecp256k1Sig
        );
        // 将可验证凭据序列化为Json字符串或二进制格式
        String vcJson = vc.exportAsJsonStr();
        byte[] vcBytes = vc.exportAsBytes();

        // 反序列化Json字符串格式数据
        VerifiableCredential vcFromJson = VerifiableCredential.parseFrom(vcJson);
        // 反序列化二进制格式数据
        VerifiableCredential vcFromBytes = VerifiableCredential.parseFrom(vcBytes);

        // 向RepChain区块链注册及初始化可验证凭据状态(包括可验证凭据属性的有效性状态)
        try {
            issuer.signupVCStatus(
                    metadata.getId(), "VALID"
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 凭据持有者did
        String holderDid = "did:rep:identity-net:456"
        // 凭据持有者本次操作所用证书名称
        String holderCertName = "holder-1"
        // 凭据持有者本次操作所用私钥(pem格式字符串，也可为PrivateKey对象)
        String holderPrvKey = "-----BEGIN PRIVATE KEY-----\nMIGTAgEAMBMGByq........\n-----END PRIVATE KEY-----";
        VCHolder holder = new VCHolder(holderDid, holderCertName, holderPrvKey);
        // 可验证凭据出示元数据
        VPMetadata meta = new VPMetadata();
        meta.addType("UniversityDegreePresentation");
        meta.setId("0x123456abcdefg");
        // 凭据持有者创建可验证凭据出示对象
        // 使用验证方提供的签名挑战信息，
        VerifiablePresentation vp = holder.present(meta, vcFromJson, SignatureTools.ECSecp256k1Sig, "challenge-bcdhjs");
        /*
         * 也可一次出示多个可验证凭据
         * List<VerifiableCredential> vcList = new ArrayList<>();
         * vcList.add(vc1);
         * vcList.add(vc2);
         * VerifiablePresentation vp = holder.present(meta, vcList, SignatureTools.ECSecp256k1Sig, "challenge-bcdhjs");
         */
        // 将可验证凭据出示对象序列化为Json字符串或二进制格式
        String vpJson = vp.exportAsJsonStr();
        byte[] vpBytes = vp.exportAsBytes();

        // 反序列化Json字符串格式数据
        VerifiablePresentation vpFromJson = VerifiablePresentation.parseFrom(vpJson);
        // 反序列化二进制格式数据
        VerifiablePresentation vpFromBytes = VerifiablePresentation.parseFrom(vpBytes);

        // 选择性披露可验证凭据属性
        // 可根据属性名称或属性编号选择性披露可验证凭据属性信息
        // 避免非必要隐私信息的泄露
        String[] name = new String[]{"name", "id", "degree"};
        VerifiableCredential disclosedVC =
                holder.discloseClaimsByName(vc, name);
        VPMetadata meta4SelectiveDisclosed = new VPMetadata();
        meta4SelectiveDisclosed.addType("UniversityDegreePresentation");
        meta4SelectiveDisclosed.setId("0x23456abcdefgh");
        VerifiablePresentation vpWithSelectiveDisclosedVC =
                holder.present(meta4SelectiveDisclosed, disclosedVC, SignatureTools.ECSecp256k1Sig, "challenge-sjdsjk");

        // 凭据验证者did
        String verifierDid = "did:rep:identity-net:789"
        // 凭据验证者本次操作所用证书名称
        String verifierCertName = "verifier-1"
        // 凭据验证者本次操作所用私钥(pem格式字符串，也可为PrivateKey对象)
        String verifierPrvKey = "-----BEGIN PRIVATE KEY-----\nMIGTAgEAMBMGByq........\n-----END PRIVATE KEY-----";
        VCVerifier verifier = new VCVerifier(verifierDid, verifierCertName, verifierPrvKey);
        // 期望凭据处于的目标状态，其顺序应与可验证凭据出示信息中包含的可验证凭据的顺序一致
        String[] targetStatus = new String[]{"VALID"};
        // 凭据验证方自己之前提供给凭据持有者的签名挑战信息
        String targetChallenge = "challenge-bcdhjs";
        // 验证结果
        boolean res = verifier.verify(vpFromBytes, targetChallenge, targetStatus);
    }
}